#include "SiftMatcher.h"
#include <cmath>
#include <algorithm>
#include <float.h>

const bool SIFT_MATCHER_DEFAULT_USE_ROOT_SIFT = true;
const double SIFT_MATCHER_DEFAULT_BEST_RATIO_THRESHOLD = 0.6;

SiftMatcher::SiftMatcher() : useRootSift_(SIFT_MATCHER_DEFAULT_USE_ROOT_SIFT),
                             bestRatioThreshold_(SIFT_MATCHER_DEFAULT_BEST_RATIO_THRESHOLD) {}

SiftMatcher::SiftMatcher(const bool useRootSift, const double bestRatioThreshold) {
    setParameter(useRootSift, bestRatioThreshold);
}

void SiftMatcher::setParameter(const bool useRootSift, const double bestRatioThreshold) {
    useRootSift_ = useRootSift;
    
    if (bestRatioThreshold < 0) {
        throw rev::Exception("SiftMatcher::setParameter", "threshold of best ratio is negative");
    }
    bestRatioThreshold_ = bestRatioThreshold;
}

void SiftMatcher::match(const rev::Image<unsigned char>& firstImage,
                        const rev::Image<unsigned char>& secondImage,
                        std::vector<MatchedPoint>& matchedPoints) const
{
    // Detect SIFT keypoints
    rev::Sift sift;
    std::vector<rev::Keypoint> firstKeypoints;
    std::vector< std::vector<unsigned char> > firstDescriptors;
    sift.detect(firstImage, firstKeypoints, firstDescriptors);
    if (firstKeypoints.size() == 0) {
        throw rev::Exception("SiftMatcher::match", "SIFT keypoints cannot be detected in first image");
    }
    std::vector<rev::Keypoint> secondKeypoints;
    std::vector< std::vector<unsigned char> > secondDescriptors;
    sift.detect(secondImage, secondKeypoints, secondDescriptors);
    
    match(firstKeypoints, firstDescriptors, secondKeypoints, secondDescriptors, matchedPoints);
}

void SiftMatcher::match(const std::vector<rev::Keypoint>& firstKeypoints,
                        const std::vector< std::vector<unsigned char> >& firstDescriptors,
                        const std::vector<rev::Keypoint>& secondKeypoints,
                        const std::vector< std::vector<unsigned char> >& secondDescriptors,
                        std::vector<MatchedPoint>& matchedPoints) const
{
    // Convert to float descriptor
    std::vector< std::vector<float> > firstFloatDescriptors;
    std::vector< std::vector<float> > secondFloatDescriptors;
    if (useRootSift_) {
        convertToRootSift(firstDescriptors, firstFloatDescriptors);
        convertToRootSift(secondDescriptors, secondFloatDescriptors);
    } else {
        convertToFloatDescriptor(firstDescriptors, firstFloatDescriptors);
        convertToFloatDescriptor(secondDescriptors, secondFloatDescriptors);
    }
    
    // Matching
    std::vector<MatchedIndices> matchedIndices;
    matchKeypoint(firstFloatDescriptors, secondFloatDescriptors, matchedIndices);
    setMatchedPoints(firstKeypoints, secondKeypoints, matchedIndices, matchedPoints);
}


void SiftMatcher::convertToFloatDescriptor(const std::vector< std::vector<unsigned char> >& ucharDescriptor,
                                           std::vector< std::vector<float> >& floatDescriptors) const
{
    int pointTotal = static_cast<int>(ucharDescriptor.size());
    int descriptorDimension = static_cast<int>(ucharDescriptor[0].size());
    
    floatDescriptors.resize(pointTotal);
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        floatDescriptors[pointIndex].resize(descriptorDimension);
        for (int d = 0; d < descriptorDimension; ++d) {
            floatDescriptors[pointIndex][d] = ucharDescriptor[pointIndex][d];
        }
    }
}


void SiftMatcher::convertToRootSift(const std::vector< std::vector<unsigned char> >& siftDescriptors,
                                    std::vector< std::vector<float> >& rootSiftDescriptors) const
{
    int pointTotal = static_cast<int>(siftDescriptors.size());
    int descriptorDimension = static_cast<int>(siftDescriptors[0].size());
    
    rootSiftDescriptors.resize(pointTotal);
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        float sum = 0.0;
        for (int d = 0; d < descriptorDimension; ++d) {
            sum += siftDescriptors[pointIndex][d];
        }
        
        rootSiftDescriptors[pointIndex].resize(descriptorDimension);
        for (int d = 0; d < descriptorDimension; ++d) {
            rootSiftDescriptors[pointIndex][d] = static_cast<float>(sqrt(static_cast<double>(siftDescriptors[pointIndex][d])/sum));
        }
    }
}

void SiftMatcher::matchKeypoint(const std::vector< std::vector<float> >& firstDescriptors,
                                const std::vector< std::vector<float> >& secondDescriptors,
                                std::vector<MatchedIndices>& matchedIndices) const
{
    const double ratioThresholdSquare = bestRatioThreshold_*bestRatioThreshold_;
    
    int firstPointTotal = static_cast<int>(firstDescriptors.size());
    int secondPointTotal = static_cast<int>(secondDescriptors.size());
    int descriptorDimension = static_cast<int>(firstDescriptors[0].size());
    
    matchedIndices.clear();
    for (int firstIndex = 0; firstIndex < firstPointTotal; ++firstIndex) {
        int matchedSecondIndex = -1;
        float minDistance = FLT_MAX;
        float secondMinDistance = FLT_MAX;
        for (int secondIndex = 0; secondIndex < secondPointTotal; ++secondIndex) {
            float descriptorDistance = 0;
            int d;
            for (d = 0; d < descriptorDimension - 3; d += 4) {
                float elementDifference0 = firstDescriptors[firstIndex][d] - secondDescriptors[secondIndex][d];
                float elementDifference1 = firstDescriptors[firstIndex][d + 1] - secondDescriptors[secondIndex][d + 1];
                float elementDifference2 = firstDescriptors[firstIndex][d + 2] - secondDescriptors[secondIndex][d + 2];
                float elementDifference3 = firstDescriptors[firstIndex][d + 3] - secondDescriptors[secondIndex][d + 3];
                descriptorDistance += elementDifference0*elementDifference0
                                        + elementDifference1*elementDifference1
                                        + elementDifference2*elementDifference2
                                        + elementDifference3*elementDifference3;
                if (descriptorDistance > secondMinDistance) break;
            }
            while (descriptorDistance < secondMinDistance && d < descriptorDimension) {
                float elementDifference = firstDescriptors[firstIndex][d] - secondDescriptors[secondIndex][d];
                descriptorDistance += elementDifference*elementDifference;
                ++d;
            }
            
            if (descriptorDistance < minDistance) {
                matchedSecondIndex = secondIndex;
                secondMinDistance = minDistance;
                minDistance = descriptorDistance;
            } else if (descriptorDistance < secondMinDistance) {
                secondMinDistance = descriptorDistance;
            }
        }
        
        if (minDistance < ratioThresholdSquare*secondMinDistance) {
            MatchedIndices newMatchIndices;
            newMatchIndices.firstIndex = firstIndex;
            newMatchIndices.secondIndex = matchedSecondIndex;
            newMatchIndices.score = minDistance/secondMinDistance;
            
            matchedIndices.push_back(newMatchIndices);
        }
    }
    
    std::sort(matchedIndices.begin(), matchedIndices.end());
}

void SiftMatcher::setMatchedPoints(const std::vector<rev::Keypoint>& firstKeypoints,
                                   const std::vector<rev::Keypoint>& secondKeypoints,
                                   const std::vector<MatchedIndices>& matchedIndices,
                                   std::vector<MatchedPoint>& matchedPoints) const
{
    int matchedPointTotal = static_cast<int>(matchedIndices.size());
    matchedPoints.resize(matchedPointTotal);
    for (int i = 0; i < matchedPointTotal; ++i) {
        int firstIndex = matchedIndices[i].firstIndex;
        int secondIndex = matchedIndices[i].secondIndex;
        matchedPoints[i].set(firstKeypoints[firstIndex], secondKeypoints[secondIndex]);
    }
}
