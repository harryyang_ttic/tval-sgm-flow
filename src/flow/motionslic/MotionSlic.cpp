#include "MotionSlic.h"
#include <cmath>
#include <float.h>

// Default parameter
const int MOTIONSLIC_DEFAULT_ENERGY_TYPE = 3;
const int MOTIONSLIC_DEFAULT_ITERATION_TOTAL = 10;
const double MOTIONSLIC_DEFAULT_COMPACTNESS_WEIGHT = 3000.0;
const double MOTIONSLIC_DEFAULT_VZINDEX_WEIGHT = 30.0;
const double MOTIONSLIC_DEFAULT_NO_VZINDEX_PENALTY = 3.0;
const double MOTIONSLIC_DEFAULT_MAX_VZ_RATIO = 0.3;
const int MOTIONSLIC_DEFAULT_DISCRETIZATION_TOTAL = 256;

// Pixel offsets of 4- and 8-neighbors
const int fourNeighborTotal = 4;
const int fourNeighborOffsetX[4] = {-1, 0, 1, 0};
const int fourNeighborOffsetY[4] = { 0,-1, 0, 1};
const int eightNeighborTotal = 8;
const int eightNeighborOffsetX[8] = {-1,-1, 0, 1, 1, 1, 0,-1};
const int eightNeighborOffsetY[8] = { 0,-1,-1,-1, 0, 1, 1, 1};

// VZ-index factor
const double vzInexFactor = 256.0;

// Prototype declaration
int computeRequiredSamplingTotal(const int drawTotal, const int inlierTotal, const int pointTotal,
								 const int currentSamplingTotal, const double confidenceLevel);

MotionSlic::MotionSlic() : energyType_(MOTIONSLIC_DEFAULT_ENERGY_TYPE),
	iterationTotal_(MOTIONSLIC_DEFAULT_ITERATION_TOTAL),
	compactnessWeight_(MOTIONSLIC_DEFAULT_COMPACTNESS_WEIGHT),
	vzIndexWeight_(MOTIONSLIC_DEFAULT_VZINDEX_WEIGHT),
	noVZIndexPenalty_(MOTIONSLIC_DEFAULT_NO_VZINDEX_PENALTY),
	maxVZRatio_(MOTIONSLIC_DEFAULT_MAX_VZ_RATIO),
	discretizationTotal_(MOTIONSLIC_DEFAULT_DISCRETIZATION_TOTAL) {}

void MotionSlic::setEnergyType(const int energyType) {
	if (energyType == 0) energyType_ = 0;
	else if (energyType == 1) energyType_ = 1;
	else if (energyType == 2) energyType_ = 2;
	else energyType_ = 3;
}

void MotionSlic::setIterationTotal(const int iterationTotal) {
	if (iterationTotal < 1) {
		throw rev::Exception("MotionSlic::setIterationTotal", "the number of iterations is less than 1");
	}

	iterationTotal_ = iterationTotal;
}

void MotionSlic::setWeightParameters(const double compactnessWeight,
									 const double vzIndexWeight,
									 const double noVZIndexPenalty)
{
	if (compactnessWeight <= 0) {
		throw rev::Exception("MotionSlic::setWeightParameters", "compactness weight is less than zero");
	}
	compactnessWeight_ = compactnessWeight;

	if (vzIndexWeight < 0) {
		throw rev::Exception("MotionSlic::setWeightParameters", "weight of vz-index term is less than zero");
	}
	vzIndexWeight_ = vzIndexWeight;

	if (noVZIndexPenalty < 0) {
		throw rev::Exception("MotionSlic::setWeightParameters", "penalty value of no vz-index is less than zero");
	}
	noVZIndexPenalty_ = noVZIndexPenalty;
}

void MotionSlic::setVZRatioParameters(const double maxVZRatio, const int discretizationTotal) {
	if (maxVZRatio <= 0.0 || maxVZRatio >= 1.0) {
		throw rev::Exception("MotionSlic::setVZRatioParameters", "max value of vz-ratio is out of range");
	}
	maxVZRatio_ = maxVZRatio;

	if (discretizationTotal <= 0 || discretizationTotal%16 != 0) {
		throw rev::Exception("MotionSlic::setVZRatioParameters", "the number of discretization must be a multiple of 16");
	}
	discretizationTotal_ = discretizationTotal;
}

void MotionSlic::segment(const int superpixelTotal,
						 const rev::Image<unsigned char>& firstImage,
						 const rev::Image<unsigned char>& secondImage,
						 const rev::Image<unsigned short>& firstVZIndexShortImage,
						 const rev::Image<unsigned short>& secondVZIndexShortImage,
						 const CameraMotion& CameraMotion,
						 rev::Image<unsigned short>& segmentImage,
						 rev::Image<unsigned short>& segmentVZIndexImage,
						 rev::Image<unsigned short>& segmentFlowImage)
{
	if (superpixelTotal < 2) {
		throw rev::Exception("MotionSlic::segment", "the number of superpixel is less than 2");
	}

	setColorAndDisparity(firstImage, secondImage, firstVZIndexShortImage, secondVZIndexShortImage);
	CalibratedEpipolarFlowGeometry epipolarFlowGeometry(firstImage.width(), firstImage.height(), CameraMotion);

	initializeSeeds(superpixelTotal);
	performSegmentation(epipolarFlowGeometry);
	enforceLabelConnectivity();

	makeSegmentImage(segmentImage);
	makeVZIndexAndFlowImage(epipolarFlowGeometry, segmentVZIndexImage, segmentFlowImage);
}


void MotionSlic::setColorAndDisparity(const rev::Image<unsigned char>& firstImage,
									  const rev::Image<unsigned char>& secondImage,
									  const rev::Image<unsigned short>& firstVZIndexShortImage,
									  const rev::Image<unsigned short>& secondVZIndexShortImage)
{
	checkInputImages(firstImage, secondImage, firstVZIndexShortImage, secondVZIndexShortImage);

	setLabImages(firstImage, secondImage);
	calcFirstLabEdges();
	setVZIndexImages(firstVZIndexShortImage, secondVZIndexShortImage);
}

void MotionSlic::initializeSeeds(const int superpixelTotal) {
	setGridSeedPoint(superpixelTotal);
	perturbSeeds();
}

void MotionSlic::performSegmentation(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry) {
	for (int iterationCount = 0; iterationCount < iterationTotal_; ++iterationCount) {
		assignLabel(epipolarFlowGeometry);
		updateSeeds();
	}
}
void MotionSlic::enforceLabelConnectivity() {
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();
	int imageSize = width*height;
	int seedTotal = static_cast<int>(seeds_.size());

	const int minimumSegmentSizeThreshold = imageSize/seedTotal/4;

	std::vector<int> newLabels(width*height, -1);
	int newLabelIndex = 0;
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (newLabels[width*y + x] >= 0) continue;

			newLabels[width*y + x] = newLabelIndex;

			int adjacentLabel = 0;
			for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
				int neighborX = x + fourNeighborOffsetX[neighborIndex];
				int neighborY = y + fourNeighborOffsetY[neighborIndex];
				if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;

				if (newLabels[width*neighborY + neighborX] >= 0) adjacentLabel = newLabels[width*neighborY + neighborX];
			}

			std::vector<int> connectedXs(1);
			std::vector<int> connectedYs(1);
			connectedXs[0] = x;
			connectedYs[0] = y;
			labelConnectedPixels(x, y, newLabelIndex, newLabels, connectedXs, connectedYs);

			int segmentPixelTotal = static_cast<int>(connectedXs.size());
			if (segmentPixelTotal <= minimumSegmentSizeThreshold) {
				for (int i = 0; i < segmentPixelTotal; ++i) {
					newLabels[width*connectedYs[i] + connectedXs[i]] = adjacentLabel;
				}
				--newLabelIndex;
			}

			++newLabelIndex;
		}
	}

	labelTotal_ = newLabelIndex;
	labels_ = newLabels;
}

void MotionSlic::makeSegmentImage(rev::Image<unsigned short>& segmentImage) const {
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();
	segmentImage.resize(width, height, 1);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			segmentImage(x, y) = labels_[width*y + x];
		}
	}
}

void MotionSlic::makeVZIndexAndFlowImage(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
										 rev::Image<unsigned short>& vzIndexImage,
										 rev::Image<unsigned short>& flowImage)
{
	seeds_.resize(labelTotal_);
	updateSeedsColorAndPosition();
	estimateVZIndexPlaneParameter();
	interpolateVZIndexPlane(epipolarFlowGeometry);

	double coefficientForVZRatio = maxVZRatio_/discretizationTotal_;
	double outputVZIndexFactor = vzInexFactor/2.0;
	double maxVZIndex = 65536/outputVZIndexFactor;

	int width = firstLabImage_.width();
	int height = firstLabImage_.height();
	vzIndexImage.resize(width, height, 1);
	flowImage.resize(width, height, 3);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int seedIndex = labels_[width*y + x];
			double estimatedVZIndex = seeds_[seedIndex].vzIndexPlane[0]*x
				+ seeds_[seedIndex].vzIndexPlane[1]*y
				+ seeds_[seedIndex].vzIndexPlane[2];
			if (estimatedVZIndex > 0.0 && estimatedVZIndex < maxVZIndex) {
				vzIndexImage(x, y) = static_cast<unsigned short>(estimatedVZIndex*outputVZIndexFactor + 0.5);
			} else if (estimatedVZIndex >= maxVZIndex) {
				vzIndexImage(x, y) = 65535;
			} else {
				vzIndexImage(x, y) = 0;
				estimatedVZIndex = 0.0;
			}

			double vzRatio = estimatedVZIndex*coefficientForVZRatio;
			double disparityValue = epipolarFlowGeometry.distanceFromEpipole(x, y)*vzRatio/(1 - vzRatio);
			double secondX = epipolarFlowGeometry.noDisparityPointX(x, y) + epipolarFlowGeometry.epipolarDirectionX(x, y)*disparityValue;
			double secondY = epipolarFlowGeometry.noDisparityPointY(x, y) + epipolarFlowGeometry.epipolarDirectionY(x, y)*disparityValue;
			double flowX = secondX - x;
			double flowY = secondY - y;

			flowImage(x, y, 0) = static_cast<unsigned short>(flowX*64.0 + 32768.5);
			flowImage(x, y, 1) = static_cast<unsigned short>(flowY*64.0 + 32768.5);
			flowImage(x, y, 2) = 1;
		}
	}
}

void MotionSlic::checkInputImages(const rev::Image<unsigned char>& firstImage,
								  const rev::Image<unsigned char>& secondImage,
								  const rev::Image<unsigned short>& firstVZIndexShortImage,
								  const rev::Image<unsigned short>& secondVZIndexShortImage) const
{
	int width = firstImage.width();
	int height = firstImage.height();
	if (secondImage.width() != width || secondImage.height() != height
		|| firstVZIndexShortImage.width() != width || firstVZIndexShortImage.height() != height
		|| secondVZIndexShortImage.width() != width || secondVZIndexShortImage.height() != height)
	{
		throw rev::Exception("MotionSlic::checkInputImages", "sizes of input images are not the same");
	}

	if (firstVZIndexShortImage.channelTotal() != 1 || secondVZIndexShortImage.channelTotal() != 1) {
		throw rev::Exception("StereoSlic::checkInputImages", "vz-index image is not a single channel image");
	}
}

void MotionSlic::setLabImages(const rev::Image<unsigned char>& firstImage,
							  const rev::Image<unsigned char>& secondImage)
{
	rev::Image<unsigned char> firstRgbColorImage = rev::convertToColor(firstImage);
	rev::convertImageRGBToLab(firstRgbColorImage, firstLabImage_);

	rev::Image<unsigned char> secondRgbColorImage = rev::convertToColor(secondImage);
	rev::convertImageRGBToLab(secondRgbColorImage, secondLabImage_);
}

void MotionSlic::calcFirstLabEdges() {
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();
	firstLabEdges_.resize(width*height, 0);
	for (int y = 1; y < height-1; ++y) {
		for (int x = 1; x < width-1; ++x) {
			double dxL = firstLabImage_(x-1, y, 0) - firstLabImage_(x+1, y, 0);
			double dxA = firstLabImage_(x-1, y, 1) - firstLabImage_(x+1, y, 1);
			double dxB = firstLabImage_(x-1, y, 2) - firstLabImage_(x+1, y, 2);
			double dx = dxL*dxL + dxA*dxA + dxB*dxB;

			double dyL = firstLabImage_(x, y-1, 0) - firstLabImage_(x, y+1, 0);
			double dyA = firstLabImage_(x, y-1, 1) - firstLabImage_(x, y+1, 1);
			double dyB = firstLabImage_(x, y-1, 2) - firstLabImage_(x, y+1, 2);
			double dy = dyL*dyL + dyA*dyA + dyB*dyB;

			firstLabEdges_[width*y + x] = dx + dy;
		}
	}
}

void MotionSlic::setVZIndexImages(const rev::Image<unsigned short>& firstVZIndexShortImage,
								  const rev::Image<unsigned short>& secondVZIndexShortImage)
{
	int width = firstVZIndexShortImage.width();
	int height = firstVZIndexShortImage.height();

	firstVZIndexImage_.resize(width, height, 1);
	secondVZIndexImage_.resize(width, height, 1);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			firstVZIndexImage_(x, y) = static_cast<float>(firstVZIndexShortImage(x, y))/vzInexFactor;
			secondVZIndexImage_(x, y) = static_cast<float>(secondVZIndexShortImage(x, y))/vzInexFactor;
		}
	}
}

void MotionSlic::setGridSeedPoint(const int superpixelTotal) {
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();
	int imageSize = width*height;

	gridSize_ = sqrt(static_cast<double>(imageSize)/superpixelTotal);
	stepSize_ = static_cast<int>(gridSize_ + 2.0);
	int offsetX = static_cast<int>(gridSize_/2.0);
	int offsetY = static_cast<int>(gridSize_/2.0);

	seeds_.clear();
	for (int indexY = 0; indexY < height; ++indexY) {
		int y = static_cast<int>(gridSize_*indexY + offsetY + 0.5);
		if (y >= height) break;
		for (int indexX = 0; indexX < width; ++indexX) {
			int x = static_cast<int>(gridSize_*indexX + offsetX + 0.5);
			if (x >= width) break;

			LabXYInd newSeed;
			newSeed.color[0] = firstLabImage_(x, y, 0);
			newSeed.color[1] = firstLabImage_(x, y, 1);
			newSeed.color[2] = firstLabImage_(x, y, 2);
			newSeed.position[0] = x;
			newSeed.position[1] = y;
			seeds_.push_back(newSeed);
		}
	}
}

void MotionSlic::perturbSeeds() {
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();

	int seedTotal = static_cast<int>(seeds_.size());
	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		int originalX = seeds_[seedIndex].position[0];
		int originalY = seeds_[seedIndex].position[1];
		int originalPixelIndex = width*originalY + originalX;

		int perturbedPixelIndex = originalPixelIndex;
		for (int neighborIndex = 0; neighborIndex < eightNeighborTotal; ++neighborIndex) {
			int neighborX = originalX + eightNeighborOffsetX[neighborIndex];
			int neighborY = originalY + eightNeighborOffsetY[neighborIndex];
			if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;
			int neighborPixelIndex = width*neighborY + neighborX;

			if (firstLabEdges_[neighborPixelIndex] < firstLabEdges_[perturbedPixelIndex]) {
				perturbedPixelIndex = neighborPixelIndex;
			}
		}

		if (perturbedPixelIndex != originalPixelIndex) {
			int perturbedX = perturbedPixelIndex%width;
			int perturbedY = perturbedPixelIndex/width;

			seeds_[seedIndex].color[0] = firstLabImage_(perturbedX, perturbedY, 0);
			seeds_[seedIndex].color[1] = firstLabImage_(perturbedX, perturbedY, 1);
			seeds_[seedIndex].color[2] = firstLabImage_(perturbedX, perturbedY, 2);
			seeds_[seedIndex].position[0] = perturbedX;
			seeds_[seedIndex].position[1] = perturbedY;
		}
	}
}

void MotionSlic::assignLabel(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry) {
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();
	int seedTotal = static_cast<int>(seeds_.size());

	double coefficientForVZRatio = maxVZRatio_/discretizationTotal_;

	double positionWeight = compactnessWeight_/(stepSize_*stepSize_);

	labels_.resize(width*height, -1);
	std::vector<double> distancesToSeeds(width*height, DBL_MAX);
	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		int minX = (seeds_[seedIndex].position[0] > stepSize_) ? seeds_[seedIndex].position[0] - stepSize_ : 0;
		int maxX = (seeds_[seedIndex].position[0] < width - stepSize_) ? seeds_[seedIndex].position[0] + stepSize_ : width;
		int minY = (seeds_[seedIndex].position[1] > stepSize_) ? seeds_[seedIndex].position[1] - stepSize_ : 0;
		int maxY = (seeds_[seedIndex].position[1] < height - stepSize_) ? seeds_[seedIndex].position[1] + stepSize_ : height;

		double seedL = seeds_[seedIndex].color[0];
		double seedA = seeds_[seedIndex].color[1];
		double seedB = seeds_[seedIndex].color[2];
		double seedX = seeds_[seedIndex].position[0];
		double seedY = seeds_[seedIndex].position[1];
		double seedAlpha = seeds_[seedIndex].vzIndexPlane[0];
		double seedBeta = seeds_[seedIndex].vzIndexPlane[1];
		double seedGamma = seeds_[seedIndex].vzIndexPlane[2];

		for (int y = minY; y < maxY; ++y) {
			for (int x = minX; x < maxX; ++x) {
				double distanceFirstLab = (firstLabImage_(x, y, 0) - seedL)*(firstLabImage_(x, y, 0) - seedL)
					+ (firstLabImage_(x, y, 1) - seedA)*(firstLabImage_(x, y, 1) - seedA)
					+ (firstLabImage_(x, y, 2) - seedB)*(firstLabImage_(x, y, 2) - seedB);
				double distanceXY = (x - seedX)*(x - seedX) + (y - seedY)*(y - seedY);

				// Default distances
				double distanceSecondLab = distanceFirstLab;
				double distanceFirstVZIndex = noVZIndexPenalty_;
				double distanceSecondVZIndex = noVZIndexPenalty_;

				if (energyType_ > 0) {
					double estimatedVZIndex = seedAlpha*x + seedBeta*y + seedGamma;
					if (estimatedVZIndex > 0) {
						if (firstVZIndexImage_(x, y) > 0) {
							// distance of first disparities
							distanceFirstVZIndex = (firstVZIndexImage_(x, y) - estimatedVZIndex)*(firstVZIndexImage_(x, y) - estimatedVZIndex);
						}

						if (energyType_ > 1) {
							double vzRatio = static_cast<double>(estimatedVZIndex)*coefficientForVZRatio;
							double disparityValue = epipolarFlowGeometry.distanceFromEpipole(x, y)*vzRatio/(1 - vzRatio);
							int secondX = static_cast<int>(epipolarFlowGeometry.noDisparityPointX(x, y)
								+ epipolarFlowGeometry.epipolarDirectionX(x, y)*disparityValue + 0.5);
							int secondY = static_cast<int>(epipolarFlowGeometry.noDisparityPointY(x, y)
								+ epipolarFlowGeometry.epipolarDirectionY(x, y)*disparityValue + 0.5);

							if (secondX >= 0 && secondX < width && secondY >= 0 && secondY < height
								&& secondVZIndexImage_(secondX, secondY) > 0)
							{
								double secondVZIndexDifference = estimatedVZIndex - secondVZIndexImage_(secondX, secondY);
								if (firstVZIndexImage_(x, y) != 0 || secondVZIndexDifference >= 0) {
									// Non-occluded
									distanceSecondVZIndex = secondVZIndexDifference*secondVZIndexDifference;
									if (energyType_ > 2) {
										distanceSecondLab = (secondLabImage_(secondX, secondY, 0) - seedL)*(secondLabImage_(secondX, secondY, 0) - seedL)
											+ (secondLabImage_(secondX, secondY, 1) - seedA)*(secondLabImage_(secondX, secondY, 1) - seedA)
											+ (secondLabImage_(secondX, secondY, 2) - seedB)*(secondLabImage_(secondX, secondY, 2) - seedB);
									}
								}
							}
						}
					}
				}

				double distanceLab = distanceFirstLab + distanceSecondLab;
				double distanceVZIndex = distanceFirstVZIndex + distanceSecondVZIndex;

				double distanceLabXYInd = distanceLab + positionWeight*distanceXY + vzIndexWeight_*distanceVZIndex;
				if (distanceLabXYInd < distancesToSeeds[width*y + x]) {
					distancesToSeeds[width*y + x] = distanceLabXYInd;
					labels_[width*y + x] = seedIndex;
				}
			}
		}
	}
}

void MotionSlic::updateSeeds() {
	updateSeedsColorAndPosition();
	if (energyType_ > 0) {
		estimateVZIndexPlaneParameter();
	}
}

void MotionSlic::updateSeedsColorAndPosition() {
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();
	int seedTotal = static_cast<int>(seeds_.size());

	std::vector<int> segmentSizes(seedTotal, 0);
	std::vector<LabXYInd> segmentSigmas(seedTotal);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int pixelSeedIndex = labels_[width*y + x];
			if (pixelSeedIndex >= 0) {
				segmentSigmas[pixelSeedIndex].color[0] += firstLabImage_(x, y, 0);
				segmentSigmas[pixelSeedIndex].color[1] += firstLabImage_(x, y, 1);
				segmentSigmas[pixelSeedIndex].color[2] += firstLabImage_(x, y, 2);
				segmentSigmas[pixelSeedIndex].position[0] += x;
				segmentSigmas[pixelSeedIndex].position[1] += y;
				++segmentSizes[pixelSeedIndex];
			}
		}
	}

	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		double segmentSizeInverse = 1.0;
		if (segmentSizes[seedIndex] > 0) segmentSizeInverse = 1.0/segmentSizes[seedIndex];

		seeds_[seedIndex].color[0] = segmentSigmas[seedIndex].color[0]*segmentSizeInverse;
		seeds_[seedIndex].color[1] = segmentSigmas[seedIndex].color[1]*segmentSizeInverse;
		seeds_[seedIndex].color[2] = segmentSigmas[seedIndex].color[2]*segmentSizeInverse;
		seeds_[seedIndex].position[0] = segmentSigmas[seedIndex].position[0]*segmentSizeInverse;
		seeds_[seedIndex].position[1] = segmentSigmas[seedIndex].position[1]*segmentSizeInverse;
	}
}

void MotionSlic::estimateVZIndexPlaneParameter() {
	std::vector< std::vector<VZIndexPixel> > segmentVZIndexPixels = makeVZIndexPixelList();
	int seedTotal = static_cast<int>(segmentVZIndexPixels.size());

	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		std::vector<double> planeParameter = estimateVZIndexPlaneParameter(segmentVZIndexPixels[seedIndex]);

		seeds_[seedIndex].vzIndexPlane[0] = planeParameter[0];
		seeds_[seedIndex].vzIndexPlane[1] = planeParameter[1];
		seeds_[seedIndex].vzIndexPlane[2] = planeParameter[2];
	}
}

std::vector< std::vector<MotionSlic::VZIndexPixel> > MotionSlic::makeVZIndexPixelList() const {
	int width = firstVZIndexImage_.width();
	int height = firstVZIndexImage_.height();
	int seedTotal = static_cast<int>(seeds_.size());

	std::vector< std::vector<VZIndexPixel> > segmentVZIndexPixels(seedTotal);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (firstVZIndexImage_(x, y) > 0) {
				int pixelSeedIndex = labels_[width*y + x];
				if (pixelSeedIndex >= 0) {
					VZIndexPixel newVZIndexPixel;
					newVZIndexPixel.x = x;
					newVZIndexPixel.y = y;
					newVZIndexPixel.vzIndex = firstVZIndexImage_(x, y);
					segmentVZIndexPixels[pixelSeedIndex].push_back(newVZIndexPixel);
				}
			}
		}
	}

	return segmentVZIndexPixels;
}

std::vector<double> MotionSlic::estimateVZIndexPlaneParameter(const std::vector<VZIndexPixel>& vzIndexPixels) const {
	std::vector<double> planeParameter(3);

	int pixelTotal = static_cast<int>(vzIndexPixels.size());
	if (pixelTotal < 3) {
		planeParameter[0] = 0;
		planeParameter[1] = 0;
		planeParameter[2] = -1;
	} else {
		bool isSameX = true;
		bool isSameY = true;
		for (int pixelIndex = 1; pixelIndex < pixelTotal; ++pixelIndex) {
			if (vzIndexPixels[pixelIndex].x != vzIndexPixels[0].x) isSameX = false;
			if (vzIndexPixels[pixelIndex].y != vzIndexPixels[0].y) isSameY = false;
			if (!isSameX && !isSameY) break;
		}
		if (isSameX || isSameY) {
			planeParameter[0] = 0;
			planeParameter[1] = 0;
			planeParameter[2] = -1;
		} else {
			planeParameter = estimateVZIndexPlaneParameterRANSAC(vzIndexPixels);
		}
	}

	return planeParameter;
}

std::vector<double> MotionSlic::estimateVZIndexPlaneParameterRANSAC(const std::vector<VZIndexPixel>& vzIndexPixels) const {
	const double inlierThreshold = 3.0;
	const double confidenceLevel = 0.99;

	int pixelTotal = static_cast<int>(vzIndexPixels.size());

	int samplingTotal = pixelTotal*2;

	int bestInlierTotal = 0;
	std::vector<bool> bestInlierFlags(pixelTotal);
	int samplingCount = 0;
	while (samplingCount < samplingTotal) {
		// Randomly select 3 pixels
		int drawIndices[3];
		drawIndices[0] = rand()%pixelTotal;
		drawIndices[1] = rand()%pixelTotal;
		while(drawIndices[1] == drawIndices[0]) drawIndices[1] = rand()%pixelTotal;
		drawIndices[2] = rand()%pixelTotal;
		while(drawIndices[2] == drawIndices[1] || drawIndices[2] == drawIndices[0]
		|| (vzIndexPixels[drawIndices[0]].x == vzIndexPixels[drawIndices[1]].x && vzIndexPixels[drawIndices[0]].x == vzIndexPixels[drawIndices[2]].x)
			|| (vzIndexPixels[drawIndices[0]].y == vzIndexPixels[drawIndices[1]].y && vzIndexPixels[drawIndices[0]].y == vzIndexPixels[drawIndices[2]].y))
		{
			drawIndices[2] = rand()%pixelTotal;
		}

		// Compute plane parameters
		std::vector<double> planeParameter;
		solvePlaneEquations(vzIndexPixels[drawIndices[0]].x, vzIndexPixels[drawIndices[0]].y, 1, vzIndexPixels[drawIndices[0]].vzIndex,
			vzIndexPixels[drawIndices[1]].x, vzIndexPixels[drawIndices[1]].y, 1, vzIndexPixels[drawIndices[1]].vzIndex,
			vzIndexPixels[drawIndices[2]].x, vzIndexPixels[drawIndices[2]].y, 1, vzIndexPixels[drawIndices[2]].vzIndex,
			planeParameter);

		// Count the number of inliers
		int inlierTotal = 0;
		std::vector<bool> inlierFlags(pixelTotal);
		for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
			double estimatedDisparity = planeParameter[0]*vzIndexPixels[pixelIndex].x
				+ planeParameter[1]*vzIndexPixels[pixelIndex].y
				+ planeParameter[2];
			if (fabs(estimatedDisparity - vzIndexPixels[pixelIndex].vzIndex) <= inlierThreshold) {
				++inlierTotal;
				inlierFlags[pixelIndex] = true;
			} else {
				inlierFlags[pixelIndex] = false;
			}
		}

		// Update best inliers
		if (inlierTotal > bestInlierTotal) {
			bestInlierTotal = inlierTotal;
			bestInlierFlags = inlierFlags;

			samplingTotal = computeRequiredSamplingTotal(3, bestInlierTotal, pixelTotal, samplingTotal, confidenceLevel);
		}

		// Increment
		++samplingCount;
	}

	// Least-square estimation
	double sumXSqr = 0, sumYSqr = 0, sumXY = 0, sumX = 0, sumY = 0;
	double sumXD = 0, sumYD = 0, sumD = 0;
	int inlierIndex = 0;
	for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
		if (bestInlierFlags[pixelIndex]) {
			sumXSqr += vzIndexPixels[pixelIndex].x*vzIndexPixels[pixelIndex].x;
			sumYSqr += vzIndexPixels[pixelIndex].y*vzIndexPixels[pixelIndex].y;
			sumXY += vzIndexPixels[pixelIndex].x*vzIndexPixels[pixelIndex].y;
			sumX += vzIndexPixels[pixelIndex].x;
			sumY += vzIndexPixels[pixelIndex].y;
			sumXD += vzIndexPixels[pixelIndex].x*vzIndexPixels[pixelIndex].vzIndex;
			sumYD += vzIndexPixels[pixelIndex].y*vzIndexPixels[pixelIndex].vzIndex;
			sumD += vzIndexPixels[pixelIndex].vzIndex;
			++inlierIndex;
		}
	}
	std::vector<double> planeParameter(3);
	solvePlaneEquations(sumXSqr, sumXY, sumX, sumXD,
		sumXY, sumYSqr, sumY, sumYD,
		sumX, sumY, inlierIndex, sumD,
		planeParameter);

	return planeParameter;
}

void MotionSlic::solvePlaneEquations(const double x1, const double y1, const double z1, const double d1,
									 const double x2, const double y2, const double z2, const double d2,
									 const double x3, const double y3, const double z3, const double d3,
									 std::vector<double>& planeParameter) const
{
	const double epsilonValue = 1e-10;

	planeParameter.resize(3);

	double denominatorA = (x1*z2 - x2*z1)*(y2*z3 - y3*z2) - (x2*z3 - x3*z2)*(y1*z2 - y2*z1);
	if (denominatorA < epsilonValue) {
		planeParameter[0] = 0.0;
		planeParameter[1] = 0.0;
		planeParameter[2] = -1.0;
		return;
	}

	planeParameter[0] = ((z2*d1 - z1*d2)*(y2*z3 - y3*z2) - (z3*d2 - z2*d3)*(y1*z2 - y2*z1))/denominatorA;

	double denominatorB = y1*z2 - y2*z1;
	if (denominatorB > epsilonValue) {
		planeParameter[1] = (z2*d1 - z1*d2 - planeParameter[0]*(x1*z2 - x2*z1))/denominatorB;
	} else {
		denominatorB = y2*z3 - y3*z2;
		planeParameter[1] = (z3*d2 - z2*d3 - planeParameter[0]*(x2*z3 - x3*z2))/denominatorB;
	}
	if (z1 > epsilonValue) {
		planeParameter[2] = (d1 - planeParameter[0]*x1 - planeParameter[1]*y1)/z1;
	} else if (z2 > epsilonValue) {
		planeParameter[2] = (d2 - planeParameter[0]*x2 - planeParameter[1]*y2)/z2;
	} else {
		planeParameter[2] = (d3 - planeParameter[0]*x3 - planeParameter[1]*y3)/z3;
	}
}

void MotionSlic::labelConnectedPixels(const int x, const int y, const int newLabelIndex,
									  std::vector<int>& newLabels, std::vector<int>& connectedXs, std::vector<int>& connectedYs) const
{
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();

	int originalLabelIndex = labels_[width*y + x];
	for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
		int neighborX = x + fourNeighborOffsetX[neighborIndex];
		int neighborY = y + fourNeighborOffsetY[neighborIndex];
		if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;

		if (newLabels[width*neighborY + neighborX] < 0 && labels_[width*neighborY + neighborX] == originalLabelIndex) {
			connectedXs.push_back(neighborX);
			connectedYs.push_back(neighborY);
			newLabels[width*neighborY + neighborX] = newLabelIndex;
			labelConnectedPixels(neighborX, neighborY, newLabelIndex, newLabels, connectedXs, connectedYs);
		}
	}
}

void MotionSlic::interpolateVZIndexPlane(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry) {
	int width = firstLabImage_.width();
	int height = firstLabImage_.height();

	int seedTotal = static_cast<int>(seeds_.size());
	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		if (seeds_[seedIndex].vzIndexPlane[0] != 0
			|| seeds_[seedIndex].vzIndexPlane[1] != 0
			|| seeds_[seedIndex].vzIndexPlane[2] != -1) continue;


		int centerX = static_cast<int>(seeds_[seedIndex].position[0] + 0.5);
		int centerY = static_cast<int>(seeds_[seedIndex].position[1] + 0.5);

		double searchDirectionX = -epipolarFlowGeometry.epipolarDirectionX(centerX, centerY);
		double searchDirectionY = -epipolarFlowGeometry.epipolarDirectionY(centerX, centerY);

		double searchX = centerX;
		double searchY = centerY;
		int sx = static_cast<int>(searchX + 0.5);
		int sy = static_cast<int>(searchY + 0.5);
		while (sx >= 0 && sx < width && sy >= 0 && sy < height
			&& seeds_[labels_[width*sy + sx]].vzIndexPlane[0] == 0
			&& seeds_[labels_[width*sy + sx]].vzIndexPlane[1] == 0
			&& seeds_[labels_[width*sy + sx]].vzIndexPlane[2] == -1)
		{
			searchX += searchDirectionX;
			searchY += searchDirectionY;
			sx = static_cast<int>(searchX + 0.5);
			sy = static_cast<int>(searchY + 0.5);
		}

		if (sx >= 0 && sx < width && sy >= 0 && sy < height) {
			int interpolateIndex = labels_[width*sy + sx];
			seeds_[seedIndex].vzIndexPlane[0] = seeds_[interpolateIndex].vzIndexPlane[0];
			seeds_[seedIndex].vzIndexPlane[1] = seeds_[interpolateIndex].vzIndexPlane[1];
			seeds_[seedIndex].vzIndexPlane[2] = seeds_[interpolateIndex].vzIndexPlane[2];
		}
	}
}


int computeRequiredSamplingTotal(const int drawTotal, const int inlierTotal, const int pointTotal, const int currentSamplingTotal, const double confidenceLevel) {
	double ep = 1 - static_cast<double>(inlierTotal)/static_cast<double>(pointTotal);
	if (ep == 1.0) {
		ep = 0.5;
	}

	int newSamplingTotal = static_cast<int>(log(1 - confidenceLevel)/log(1 - pow(1 - ep, drawTotal)) + 0.5);
	if (newSamplingTotal < currentSamplingTotal) {
		return newSamplingTotal;
	} else {
		return currentSamplingTotal;
	}
}
