#include "CalibratedEpipolarFlowGeometry.h"

CalibratedEpipolarFlowGeometry::CalibratedEpipolarFlowGeometry(const int imageWidth, const int imageHeight, const CameraMotion& cameraMotion) {
	buildTransformationImages(imageWidth, imageHeight, cameraMotion);
}

void CalibratedEpipolarFlowGeometry::buildTransformationImages(const int imageWidth, const int imageHeight, const CameraMotion& cameraMotion) {
	Eigen::Matrix3d homographyMatrix = cameraMotion.rotationHomographyMatrix();

	double epipoleX, epipoleY;
	cameraMotion.computeSecondEpipole(epipoleX, epipoleY);

	distancesFromEpipole_.resize(imageWidth, imageHeight, 1);
	noDisparityPoints_.resize(imageWidth, imageHeight, 2);
	epipolarDirectionVectors_.resize(imageWidth, imageHeight, 2);

	for (int y = 0; y < imageHeight; ++y) {
		for (int x = 0; x < imageWidth; ++x) {
			Eigen::Vector3d imagePoint;
			imagePoint << x, y, 1;
			Eigen::Vector3d rotatedPoint = homographyMatrix*imagePoint;
			rotatedPoint /= rotatedPoint(2);

			Eigen::Vector3d epipolarLine;
			cameraMotion.computeSecondEpipolarLine(x, y, epipolarLine);
			double coefficientToEpipolarLine = -epipolarLine(0)*rotatedPoint(0) - epipolarLine(1)*rotatedPoint(1) - epipolarLine(2);
			double noDisparityPointX = rotatedPoint(0) + epipolarLine(0)*coefficientToEpipolarLine;
			double noDisparityPointY = rotatedPoint(1) + epipolarLine(1)*coefficientToEpipolarLine;

			double distanceFromEpipole = sqrt((noDisparityPointX - epipoleX)*(noDisparityPointX - epipoleX)
				+ (noDisparityPointY - epipoleY)*(noDisparityPointY - epipoleY));

			double epipolarDirectionX = -epipolarLine(1);
			double epipolarDirectionY = epipolarLine(0);
			if (cameraMotion.epipolarSearchDirectionFlag() == 0) {
				if ((noDisparityPointX < epipoleX && epipolarDirectionX > 0)
					|| (noDisparityPointX > epipoleX && epipolarDirectionX < 0))
				{
					epipolarDirectionX *= -1.0;
					epipolarDirectionY *= -1.0;
				}
			} else {
				if ((noDisparityPointX < epipoleX && epipolarDirectionX < 0)
					|| (noDisparityPointX > epipoleX && epipolarDirectionX > 0))
				{
					epipolarDirectionX *= -1.0;
					epipolarDirectionY *= -1.0;
				}
			}

			distancesFromEpipole_(x, y) = distanceFromEpipole;
			noDisparityPoints_(x, y, 0) = noDisparityPointX;
			noDisparityPoints_(x, y, 1) = noDisparityPointY;
			epipolarDirectionVectors_(x, y, 0) = epipolarDirectionX;
			epipolarDirectionVectors_(x, y, 1) = epipolarDirectionY;
		}
	}
}
