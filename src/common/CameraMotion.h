#pragma once

#include <string>
#include <vector>
#include <eigen3/Eigen/Dense>
#include "TriMatchedPoint.h"

class CameraMotion {
public:
	CameraMotion() : stereoBaseline_(0), ratioVzDisparityConstant_(0), ratioVzDisparityGradientX_(0), ratioVzDisparityGradientY_(0) {}

	void initialize(const std::string calibrationFilename, const std::string fundamentalMatrixFilename);

	void estimateRatioVzDisparity(std::vector<TriMatchedPoint>& matchedPoints);

	void computeFirstEpipolarLine(const double secondX, const double secondY, Eigen::Vector3d& firstEpipolarLine) const;
	void computeSecondEpipolarLine(const double firstX, const double firstY, Eigen::Vector3d& secondEpipolarLine) const;
	void computeFirstEpipole(double& firstEpipoleX, double& firstEpipoleY) const;
	void computeSecondEpipole(double& secondEpipoleX, double& secondEpipoleY) const;

	Eigen::Matrix3d calibrationMatrix() const { return calibrationMatrix_; }
	double stereoBaseline() const { return stereoBaseline_; }
	Eigen::Matrix3d fundamentalMatrix() const { return fundamentalMatrix_; }
	double epipolarSearchDirectionFlag() const { return epipolarSearchDirectionFlag_; }
	Eigen::Matrix3d rotationHomographyMatrix() const { return rotationHomographyMatrix_; }
	double ratioVzDisparity(const double x, const double y) const { return ratioVzDisparityConstant_ + ratioVzDisparityGradientX_*x + ratioVzDisparityGradientY_*y; }

	void writeCameraMotionFile(const std::string cameraMotionFilename) const;
	void readCameraMotionFile(const std::string cameraMotionFilename);

private:
	void readCalibrationFile(const std::string calibrationFilename);
	void readFundamentalMatrix(const std::string fundamentalMatrixFilename);
	void calcRotationHomographyMatrix();
	
	void computeWorldCoordinates(std::vector<TriMatchedPoint>& matchedPoints) const;
	void selectMatchedPoint(const std::vector<TriMatchedPoint>& matchedPoints,
		const double pointDistanceThreshold,
		std::vector<TriMatchedPoint>& selectedPoints) const;
	void estimateRatioPlaneRANSAC(const std::vector<TriMatchedPoint>& matchedPoints);
	void computeRatioVzDisparity(const std::vector<TriMatchedPoint>& matchedPoints, const double epipoleX, const double epipoleY, std::vector<double>& estimatedRatios) const;
	double computeVzRatio(const TriMatchedPoint& matchedPoint, const double epipoleX, const double epipoleY) const;
	bool computeProjectedSecondPointOnEpipolarLine(const TriMatchedPoint& matchedPoint, const Eigen::Vector3d& epipolarLine,
		double& epipolarSecondX, double& epipolarSecondY) const;
	void computeNoDisparityPoint(const TriMatchedPoint& matchedPoint, const Eigen::Vector3d& epipolarLine, const double epipoleX, const double epipoleY,
		double& noDisparityPointX, double& noDisparityPointY, double& distanceFromEpipole) const;
	void decideSignOfEpipolarDirection(const double noDisparityPointX, const double epipoleX, double& epipolarDirectionX, double& epipolarDirectionY) const;


	Eigen::Matrix3d calibrationMatrix_;
	double stereoBaseline_;
	Eigen::Matrix3d fundamentalMatrix_;
	unsigned char epipolarSearchDirectionFlag_;
	Eigen::Matrix3d rotationHomographyMatrix_;

	double ratioVzDisparityConstant_;
	double ratioVzDisparityGradientX_;
	double ratioVzDisparityGradientY_;
};
