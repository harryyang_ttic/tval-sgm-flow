#include "ImageProcessing.h"
#include <cmath>
#include <emmintrin.h>

namespace rev {
    
// Prototype declaration
void initializeLabConvertTable(std::vector<float>& sRGBGammaCorrections,
                               std::vector< std::vector<float> >& fXYZConversions,
                               std::vector<double>& xyzTableIndexCoefficients);
void convertColorRGB2Lab(const Color<unsigned char>& rgbColor,
                         const std::vector<float>& sRGBGammaCorrections,
                         const std::vector< std::vector<float> >& fXYZConversions,
                         const std::vector<double>& xyzTableIndexCoefficients,
                         Color<float>& labColor);
void convolveColumnSSE(float* sourceImageData, const int width, const int height, const int widthStep,
                       float* convolvedImageData,
                       const std::vector<float>& kernel, const bool padZero = false);
void convolveRowSSE(float* sourceImageData, const int width, const int height, const int widthStep,
                    float* convolvedImageData,
                    const std::vector<float>& kernel, const bool padZero = false);


void convertImageUCharToFloat(const Image<unsigned char>& inputUCharImage, Image<float>& outputFloatImage) {
    int width = inputUCharImage.width();
    int height = inputUCharImage.height();
    int channelTotal = inputUCharImage.channelTotal();
    unsigned char* inputImageData = inputUCharImage.dataPointer();
    int inputWidthStep = inputUCharImage.widthStep();

    outputFloatImage.resize(width, height, channelTotal);
    float* outputImageData = outputFloatImage.dataPointer();
    int outputWidthStep = outputFloatImage.widthStep();
    
    for (int y = 0; y < height; ++y) {
        unsigned char* inputPixel = inputImageData + inputWidthStep*y;
        float* outputPixel = outputImageData + outputWidthStep*y;
        for (int i = 0; i < width*channelTotal; ++i) {
            *(outputPixel) = static_cast<float>(*(inputPixel));
            ++inputPixel;
            ++outputPixel;
        }
    }
}
    
void convertImageFloatToUChar(const Image<float>& inputFloatImage, Image<unsigned char>& outputUCharImage) {
    int width = inputFloatImage.width();
    int height = inputFloatImage.height();
    int channelTotal = inputFloatImage.channelTotal();
    float* inputImageData = inputFloatImage.dataPointer();
    int inputWidthStep = inputFloatImage.widthStep();
    
    outputUCharImage.resize(width, height, channelTotal);
    unsigned char* outputImageData = outputUCharImage.dataPointer();
    int outputWidthStep = outputUCharImage.widthStep();
    
    for (int y = 0; y < height; ++y) {
        float* inputPixel = inputImageData + inputWidthStep*y;
        unsigned char* outputPixel = outputImageData + outputWidthStep*y;
        for (int i = 0;i < width*channelTotal; ++i) {
            *(outputPixel) = static_cast<unsigned char>(*(inputPixel) + 0.5);
            ++inputPixel;
            ++outputPixel;
        }
    }
}

void convertImageRGBToLab(const Image<unsigned char>& rgbImage, Image<float>& labImage) {
    if (rgbImage.channelTotal() != 3) {
        throw Exception("convertImageRGBToLab", "image is not a color image");
    }
    
    std::vector<float> sRGBGammaCorrections;
    std::vector< std::vector<float> > fXYZConversions;
    std::vector<double> xyzTableIndexCoefficients;
    initializeLabConvertTable(sRGBGammaCorrections, fXYZConversions, xyzTableIndexCoefficients);
    
    int width = rgbImage.width();
    int height = rgbImage.height();
    labImage.resize(width, height, 3);
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            Color<unsigned char> rgbColor = rgbImage.getColor(x, y);
            Color<float> labColor;
            convertColorRGB2Lab(rgbColor, sRGBGammaCorrections, fXYZConversions, xyzTableIndexCoefficients, labColor);
            labImage.setColor(x, y, labColor);
        }
    }
}
    
void gaussianSmooth(const Image<float>& sourceImage, const double sigma, Image<float>& gaussianImage) {
    if (sourceImage.channelTotal() != 1) {
        throw Exception("rev::gaussianSmooth", "the number of channels must be 1");
    }
    if (sigma == 0) {
        gaussianImage = sourceImage;
        return;
    }
    if (sigma < 0) {
        throw Exception("rev::gaussianSmooth", "sigma must be larger than zero");
    }
        
    int kernelRadius = ceil(4.0*sigma);
    if (kernelRadius < 1) kernelRadius = 1;
    int kernelSize = 2*kernelRadius + 1;
    std::vector<float> gaussianKernel(kernelSize);
    
    float kernelSum = 0.0;
    for (int i = 0; i < kernelSize; ++i) {
        float d = static_cast<float>(i - kernelRadius)/static_cast<float>(sigma);
        gaussianKernel[i] = static_cast<float>(exp(-0.5*(d*d)));
        kernelSum += gaussianKernel[i];
    }
    for (int i = 0; i < kernelSize; ++i) {
        gaussianKernel[i] /= kernelSum;
    }

    int width = sourceImage.width();
    int height = sourceImage.height();
    int widthStep = sourceImage.widthStep();
    float* sourceImageData = sourceImage.dataPointer();
    
    Image<float> columnFilteredImage(width, height, 1);
    float* columnFilteredImageData = columnFilteredImage.dataPointer();
    convolveColumnSSE(sourceImageData, width, height, widthStep, columnFilteredImageData, gaussianKernel, false);
    gaussianImage.resize(width, height, 1);
    float* gaussianImageData = gaussianImage.dataPointer();
    convolveRowSSE(columnFilteredImageData, width, height, widthStep, gaussianImageData, gaussianKernel, false);
}

    
void initializeLabConvertTable(std::vector<float>& sRGBGammaCorrections,
                               std::vector< std::vector<float> >& fXYZConversions,
                               std::vector<double>& xyzTableIndexCoefficients)
{
    sRGBGammaCorrections.resize(256);
    for (int pixelValue = 0; pixelValue < 256; ++pixelValue) {
        double normalizedValue = pixelValue/255.0;
        double transformedValue = (normalizedValue <= 0.04045) ? normalizedValue/12.92 : pow((normalizedValue+0.055)/1.055, 2.4);
        
        sRGBGammaCorrections[pixelValue] = transformedValue;
    }

    const int RGB2LABCONVERTER_XYZ_TABLE_SIZE = 1024;
    // CIE standard parameters
    const double epsilon = 0.008856;
    const double kappa = 903.3;
    // Reference white
    const double referenceWhite[3] = {0.950456, 1.0, 1.088754};
    /// Maximum values
    const double maxXYZValues[3] = {0.95047, 1.0, 1.08883};
        
    int tableSize = RGB2LABCONVERTER_XYZ_TABLE_SIZE;
    xyzTableIndexCoefficients.resize(3);
    xyzTableIndexCoefficients[0] = (tableSize-1)/maxXYZValues[0];
    xyzTableIndexCoefficients[1] = (tableSize-1)/maxXYZValues[1];
    xyzTableIndexCoefficients[2] = (tableSize-1)/maxXYZValues[2];
        
    fXYZConversions.resize(3);
    for (int xyzIndex = 0; xyzIndex < 3; ++xyzIndex) {
        fXYZConversions[xyzIndex].resize(tableSize);
        double stepValue = maxXYZValues[xyzIndex]/tableSize;
        for (int tableIndex = 0; tableIndex < tableSize; ++tableIndex) {
            double originalValue = stepValue*tableIndex;
            double normalizedValue = originalValue/referenceWhite[xyzIndex];
            double transformedValue = (normalizedValue > epsilon) ? pow(normalizedValue, 1.0/3.0) : (kappa*normalizedValue + 16.0)/116.0;
                
            fXYZConversions[xyzIndex][tableIndex] = transformedValue;
        }
    }
}
    
void convertColorRGB2Lab(const Color<unsigned char>& rgbColor,
                         const std::vector<float>& sRGBGammaCorrections,
                         const std::vector< std::vector<float> >& fXYZConversions,
                         const std::vector<double>& xyzTableIndexCoefficients,
                         Color<float>& labColor)
{
    float correctedR = sRGBGammaCorrections[rgbColor[0]];
    float correctedG = sRGBGammaCorrections[rgbColor[1]];
    float correctedB = sRGBGammaCorrections[rgbColor[2]];
        
    Color<float> xyzColor;
    xyzColor[0] = correctedR*0.4124564f + correctedG*0.3575761f + correctedB*0.1804375f;
    xyzColor[1] = correctedR*0.2126729f + correctedG*0.7151522f + correctedB*0.0721750f;
    xyzColor[2] = correctedR*0.0193339f + correctedG*0.1191920f + correctedB*0.9503041f;
        
    int tableIndexX = static_cast<int>(xyzColor[0]*xyzTableIndexCoefficients[0] + 0.5);
    int tableIndexY = static_cast<int>(xyzColor[1]*xyzTableIndexCoefficients[1] + 0.5);
    int tableIndexZ = static_cast<int>(xyzColor[2]*xyzTableIndexCoefficients[2] + 0.5);
        
    float fX = fXYZConversions[0][tableIndexX];
    float fY = fXYZConversions[1][tableIndexY];
    float fZ = fXYZConversions[2][tableIndexZ];
        
    labColor[0] = 116.0*fY - 16.0;
    labColor[1] = 500.0*(fX - fY);
    labColor[2] = 200.0*(fY - fZ);
}
    
void convolveColumnSSE(float* sourceImageData, const int width, const int height, const int widthStep,
                       float* convolvedImageData,
                       const std::vector<float>& kernel, const bool padZero)
{
    int kernelRadius = (static_cast<int>(kernel.size()) - 1)/2;
    
    for (int y = 0; y < height; ++y) {
        float* sourceRowData = sourceImageData + widthStep*y;
        float* convolvedRowData = convolvedImageData + widthStep*y;
        
        int startOffset = -kernelRadius;
        if (y < kernelRadius) startOffset = -y;
        int endOffset = kernelRadius;
        if (y > height - kernelRadius - 1) endOffset = height - y - 1;
        
        __m128 regKernel, regSum0, regSum1, regSum2, regSum3;
        for (int x = 0; x < width; x += 16) {
            regSum0 = _mm_setzero_ps();
            regSum1 = _mm_setzero_ps();
            regSum2 = _mm_setzero_ps();
            regSum3 = _mm_setzero_ps();
            
            if (!padZero && y < kernelRadius) {
                __m128 regNegativeRow0 = _mm_load_ps(sourceImageData + x);
                __m128 regNegativeRow1 = _mm_load_ps(sourceImageData + x + 4);
                __m128 regNegativeRow2 = _mm_load_ps(sourceImageData + x + 8);
                __m128 regNegativeRow3 = _mm_load_ps(sourceImageData + x + 12);
                for (int k = -kernelRadius; k < startOffset; ++k) {
                    regKernel = _mm_set1_ps(kernel[kernelRadius + k]);
                    regSum0 = _mm_add_ps(regSum0, _mm_mul_ps(regKernel, regNegativeRow0));
                    regSum1 = _mm_add_ps(regSum1, _mm_mul_ps(regKernel, regNegativeRow1));
                    regSum2 = _mm_add_ps(regSum2, _mm_mul_ps(regKernel, regNegativeRow2));
                    regSum3 = _mm_add_ps(regSum3, _mm_mul_ps(regKernel, regNegativeRow3));
                }
            }
            for (int k = startOffset; k <= endOffset; ++k) {
                regKernel = _mm_set1_ps(kernel[kernelRadius + k]);
                regSum0 = _mm_add_ps(regSum0, _mm_mul_ps(regKernel, _mm_load_ps(sourceRowData + widthStep*k + x)));
                regSum1 = _mm_add_ps(regSum1, _mm_mul_ps(regKernel, _mm_load_ps(sourceRowData + widthStep*k + x + 4)));
                regSum2 = _mm_add_ps(regSum2, _mm_mul_ps(regKernel, _mm_load_ps(sourceRowData + widthStep*k + x + 8)));
                regSum3 = _mm_add_ps(regSum3, _mm_mul_ps(regKernel, _mm_load_ps(sourceRowData + widthStep*k + x + 12)));
            }
            if (!padZero && y > height - kernelRadius - 1) {
                float* sourceLastRowData = sourceImageData + widthStep*(height - 1);
                __m128 regOverRow0 = _mm_load_ps(sourceLastRowData + x);
                __m128 regOverRow1 = _mm_load_ps(sourceLastRowData + x + 4);
                __m128 regOverRow2 = _mm_load_ps(sourceLastRowData + x + 8);
                __m128 regOverRow3 = _mm_load_ps(sourceLastRowData + x + 12);
                for (int k = endOffset + 1; k <= kernelRadius; ++k) {
                    regKernel = _mm_set1_ps(kernel[kernelRadius + k]);
                    regSum0 = _mm_add_ps(regSum0, _mm_mul_ps(regKernel, regOverRow0));
                    regSum1 = _mm_add_ps(regSum1, _mm_mul_ps(regKernel, regOverRow1));
                    regSum2 = _mm_add_ps(regSum2, _mm_mul_ps(regKernel, regOverRow2));
                    regSum3 = _mm_add_ps(regSum3, _mm_mul_ps(regKernel, regOverRow3));
                }
                
            }
            _mm_store_ps(convolvedRowData + x, regSum0);
            _mm_store_ps(convolvedRowData + x + 4, regSum1);
            _mm_store_ps(convolvedRowData + x + 8, regSum2);
            _mm_store_ps(convolvedRowData + x + 12, regSum3);
        }
    }
}
    
void convolveRowSSE(float* sourceImageData, const int width, const int height, const int widthStep,
                    float* convolvedImageData,
                    const std::vector<float>& kernel, const bool padZero)
{
    int kernelRadius = (static_cast<int>(kernel.size()) - 1)/2;
    int sideMargin = 16*ceil(kernelRadius/16.0);
    
    for (int y = 0; y < height; ++y) {
        float* sourceRowData = sourceImageData + widthStep*y;
        float* convolvedRowData = convolvedImageData + widthStep*y;
        
        float outPixelValue = padZero ? 0 : sourceRowData[0];
        for (int x = 0; x < kernelRadius; ++x) {
            float filteredSum = 0.0;
            for (int k = -kernelRadius; k < -x; ++k) {
                filteredSum += kernel[kernelRadius + k]*outPixelValue;
            }
            for (int k = -x; k <= kernelRadius; ++k) {
                filteredSum += kernel[kernelRadius + k]*sourceRowData[x + k];
            }
            convolvedRowData[x] = filteredSum;
        }
        for (int x = kernelRadius; x < sideMargin; ++x) {
            float filteredSum = 0.0;
            for (int k = -kernelRadius; k <= kernelRadius; ++k) {
                filteredSum += kernel[kernelRadius + k]*sourceRowData[x + k];
            }
            convolvedRowData[x] = filteredSum;
        }
        
        __m128 regKernel, regSum0, regSum1, regSum2, regSum3;
        for (int x = sideMargin; x < width - sideMargin; x += 16) {
            regSum0 = _mm_setzero_ps();
            regSum1 = _mm_setzero_ps();
            regSum2 = _mm_setzero_ps();
            regSum3 = _mm_setzero_ps();
            for (int k = -kernelRadius; k <= kernelRadius; ++k) {
                regKernel = _mm_load1_ps(&kernel[kernelRadius + k]);
                regSum0 = _mm_add_ps(regSum0, _mm_mul_ps(regKernel, _mm_loadu_ps(sourceRowData + x + k)));
                regSum1 = _mm_add_ps(regSum1, _mm_mul_ps(regKernel, _mm_loadu_ps(sourceRowData + x + 4 + k)));
                regSum2 = _mm_add_ps(regSum2, _mm_mul_ps(regKernel, _mm_loadu_ps(sourceRowData + x + 8 + k)));
                regSum3 = _mm_add_ps(regSum3, _mm_mul_ps(regKernel, _mm_loadu_ps(sourceRowData + x + 12 + k)));
            }
            _mm_store_ps(convolvedRowData + x, regSum0);
            _mm_store_ps(convolvedRowData + x + 4, regSum1);
            _mm_store_ps(convolvedRowData + x + 8, regSum2);
            _mm_store_ps(convolvedRowData + x + 12, regSum3);
        }
        
        for (int x = width - sideMargin; x < width - kernelRadius; ++x) {
            float filteredSum = 0.0;
            for (int k = -kernelRadius; k <= kernelRadius; ++k) {
                filteredSum += kernel[k + kernelRadius]*sourceRowData[x + k];
            }
            convolvedRowData[x] = filteredSum;
        }
        outPixelValue = padZero ? 0 : sourceRowData[width - 1];
        for (int x = width - kernelRadius; x < width; ++x) {
            float filteredSum = 0.0;
            for (int k = -kernelRadius; k < width - x; ++k) {
                filteredSum += kernel[k + kernelRadius]*sourceRowData[x + k];
            }
            for (int k = width - x; k <= kernelRadius; ++k) {
                filteredSum += kernel[k + kernelRadius]*outPixelValue;
            }
            convolvedRowData[x] = filteredSum;
        }
     }
}

}
