#include "StereoSlic.h"
#include <cmath>
#include <float.h>

// Default parameter
const int STEREOSLIC_DEFAULT_ENERGY_TYPE = 3;
const int STEREOSLIC_DEFAULT_ITERATION_TOTAL = 10;
const double STEREOSLIC_DEFAULT_COMPACTNESS_WEIGHT = 3000.0;
const double STEREOSLIC_DEFAULT_DISPARITY_WEIGHT = 30.0;
const double STEREOSLIC_DEFAULT_NO_DISPARITY_PENALTY = 3.0;

// Pixel offsets of 4- and 8-neighbors
const int fourNeighborTotal = 4;
const int fourNeighborOffsetX[4] = {-1, 0, 1, 0};
const int fourNeighborOffsetY[4] = { 0,-1, 0, 1};
const int eightNeighborTotal = 8;
const int eightNeighborOffsetX[8] = {-1,-1, 0, 1, 1, 1, 0,-1};
const int eightNeighborOffsetY[8] = { 0,-1,-1,-1, 0, 1, 1, 1};

// Prototype declaration
int computeRequiredSamplingTotal(const int drawTotal, const int inlierTotal, const int pointTotal,
                                 const int currentSamplingTotal, const double confidenceLevel);


StereoSlic::StereoSlic() : energyType_(STEREOSLIC_DEFAULT_ENERGY_TYPE),
                           iterationTotal_(STEREOSLIC_DEFAULT_ITERATION_TOTAL),
                           compactnessWeight_(STEREOSLIC_DEFAULT_COMPACTNESS_WEIGHT),
                           disparityWeight_(STEREOSLIC_DEFAULT_DISPARITY_WEIGHT),
                           noDisparityPenalty_(STEREOSLIC_DEFAULT_NO_DISPARITY_PENALTY) {}

void StereoSlic::setEnergyType(const int energyType) {
    if (energyType == 0) energyType_ = 0;
    else if (energyType == 1) energyType_ = 1;
    else if (energyType == 2) energyType_ = 2;
    else energyType_ = 3;
}

void StereoSlic::setIterationTotal(const int iterationTotal) {
    if (iterationTotal < 1) {
        throw rev::Exception("StereoSlic::setIterationTotal", "the number of iterations is less than 1");
    }
    
    iterationTotal_ = iterationTotal;
}

void StereoSlic::setWeightParameters(const double compactnessWeight,
                                     const double disparityWeight,
                                     const double noDisparityPenalty)
{
    if (compactnessWeight <= 0) {
        throw rev::Exception("StereoSlic::setWeightParameters", "compactness weight is less than zero");
    }
    compactnessWeight_ = compactnessWeight;

    if (disparityWeight < 0) {
        throw rev::Exception("StereoSlic::setWeightParameters", "weight of disparity term is less than zero");
    }
    disparityWeight_ = disparityWeight;

    if (noDisparityPenalty < 0) {
        throw rev::Exception("StereoSlic::setWeightParameters", "penalty value of no disparity is less than zero");
    }
    noDisparityPenalty_ = noDisparityPenalty;
}

void StereoSlic::segment(const int superpixelTotal,
                         const rev::Image<unsigned char>& leftImage,
                         const rev::Image<unsigned char>& rightImage,
                         const rev::Image<unsigned short>& leftDisparityShortImage,
                         const rev::Image<unsigned short>& rightDisparityShortImage,
                         const double disparityFactor,
                         rev::Image<unsigned short>& segmentImage,
                         rev::Image<unsigned short>& segmentDisparityImage)
{
    if (superpixelTotal < 2) {
        throw rev::Exception("StereoSlic::segment", "the number of superpixel is less than 2");
    }
    
    setColorAndDisparity(leftImage, rightImage, leftDisparityShortImage, rightDisparityShortImage, disparityFactor);
    
    initializeSeeds(superpixelTotal);
    performSegmentation();
    enforceLabelConnectivity();
    
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    segmentImage.resize(width, height, 1);
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            segmentImage(x, y) = labels_[width*y + x];
        }
    }
    
    makeDisparityImage(disparityFactor, segmentDisparityImage);
}


void StereoSlic::setColorAndDisparity(const rev::Image<unsigned char>& leftImage,
                                      const rev::Image<unsigned char>& rightImage,
                                      const rev::Image<unsigned short>& leftDisparityShortImage,
                                      const rev::Image<unsigned short>& rightDisparityShortImage,
                                      const double disparityFactor)
{
    checkInputImages(leftImage, rightImage, leftDisparityShortImage, rightDisparityShortImage);
    
    setStereoLabImages(leftImage, rightImage);
    calcLeftLabEdges();
    setDisparityImages(leftDisparityShortImage, rightDisparityShortImage, disparityFactor);
}

void StereoSlic::initializeSeeds(const int superpixelTotal) {
    setGridSeedPoint(superpixelTotal);
    perturbSeeds();
}

void StereoSlic::performSegmentation() {
    for (int iterationCount = 0; iterationCount < iterationTotal_; ++iterationCount) {
        assignLabel();
        updateSeeds();
    }
}

void StereoSlic::enforceLabelConnectivity() {
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    int imageSize = width*height;
    int seedTotal = static_cast<int>(seeds_.size());
    
    const int minimumSegmentSizeThreshold = imageSize/seedTotal/4;
    
    std::vector<int> newLabels(width*height, -1);
    int newLabelIndex = 0;
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            if (newLabels[width*y + x] >= 0) continue;
            
            newLabels[width*y + x] = newLabelIndex;
            
            int adjacentLabel = 0;
            for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
                int neighborX = x + fourNeighborOffsetX[neighborIndex];
                int neighborY = y + fourNeighborOffsetY[neighborIndex];
                if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;
                
                if (newLabels[width*neighborY + neighborX] >= 0) adjacentLabel = newLabels[width*neighborY + neighborX];
            }
            
            std::vector<int> connectedXs(1);
            std::vector<int> connectedYs(1);
            connectedXs[0] = x;
            connectedYs[0] = y;
            labelConnectedPixels(x, y, newLabelIndex, newLabels, connectedXs, connectedYs);
            
            int segmentPixelTotal = static_cast<int>(connectedXs.size());
            if (segmentPixelTotal <= minimumSegmentSizeThreshold) {
                for (int i = 0; i < segmentPixelTotal; ++i) {
                    newLabels[width*connectedYs[i] + connectedXs[i]] = adjacentLabel;
                }
                --newLabelIndex;
            }
            
            ++newLabelIndex;
        }
    }
    
    labelTotal_ = newLabelIndex;
    labels_ = newLabels;
}

void StereoSlic::makeDisparityImage(const double disparityFactor, rev::Image<unsigned short>& disparityImage) {
    seeds_.resize(labelTotal_);
    estimateDisparityPlaneParameter();
    
    double maxDisparity = 65536/disparityFactor;
    
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    disparityImage.resize(width, height, 1);
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            int seedIndex = labels_[width*y + x];
            double estimatedDisparity = seeds_[seedIndex].disparityPlane[0]*x
                                        + seeds_[seedIndex].disparityPlane[1]*y
                                        + seeds_[seedIndex].disparityPlane[2];
            if (estimatedDisparity > 0.0 && estimatedDisparity < maxDisparity) {
                disparityImage(x, y) = static_cast<unsigned short>(estimatedDisparity*disparityFactor + 0.5);
            } else if (estimatedDisparity >= maxDisparity) {
                disparityImage(x, y) = 65535;
            } else {
                disparityImage(x, y) = 0;
            }
        }
    }
}


void StereoSlic::checkInputImages(const rev::Image<unsigned char>& leftImage,
                                  const rev::Image<unsigned char>& rightImage,
                                  const rev::Image<unsigned short>& leftDisparityShortImage,
                                  const rev::Image<unsigned short>& rightDisparityShortImage) const
{
    int width = leftImage.width();
    int height = leftImage.height();
    if (rightImage.width() != width || rightImage.height() != height
        || leftDisparityShortImage.width() != width || leftDisparityShortImage.height() != height
        || rightDisparityShortImage.width() != width || rightDisparityShortImage.height() != height)
    {
        throw rev::Exception("StereoSlic::checkInputImages", "sizes of input images are not the same");
    }
    
    if (leftDisparityShortImage.channelTotal() != 1 || rightDisparityShortImage.channelTotal() != 1) {
        throw rev::Exception("StereoSlic::checkInputImages", "disparity image is not a single channel image");
    }
}

void StereoSlic::setStereoLabImages(const rev::Image<unsigned char>& leftImage,
                                    const rev::Image<unsigned char>& rightImage)
{
    rev::Image<unsigned char> leftRgbColorImage = rev::convertToColor(leftImage);
    rev::convertImageRGBToLab(leftRgbColorImage, leftLabImage_);
    
    rev::Image<unsigned char> rightRgbColorImage = rev::convertToColor(rightImage);
    rev::convertImageRGBToLab(rightRgbColorImage, rightLabImage_);
}

void StereoSlic::calcLeftLabEdges() {
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    leftLabEdges_.resize(width*height, 0);
    for (int y = 1; y < height-1; ++y) {
        for (int x = 1; x < width-1; ++x) {
            double dxL = leftLabImage_(x-1, y, 0) - leftLabImage_(x+1, y, 0);
            double dxA = leftLabImage_(x-1, y, 1) - leftLabImage_(x+1, y, 1);
            double dxB = leftLabImage_(x-1, y, 2) - leftLabImage_(x+1, y, 2);
            double dx = dxL*dxL + dxA*dxA + dxB*dxB;
            
            double dyL = leftLabImage_(x, y-1, 0) - leftLabImage_(x, y+1, 0);
            double dyA = leftLabImage_(x, y-1, 1) - leftLabImage_(x, y+1, 1);
            double dyB = leftLabImage_(x, y-1, 2) - leftLabImage_(x, y+1, 2);
            double dy = dyL*dyL + dyA*dyA + dyB*dyB;
            
            leftLabEdges_[width*y + x] = dx + dy;
        }
    }
}

void StereoSlic::setDisparityImages(const rev::Image<unsigned short>& leftDisparityShortImage,
                                    const rev::Image<unsigned short>& rightDisparityShortImage,
                                    const double disparityFactor)
{
    int width = leftDisparityShortImage.width();
    int height = leftDisparityShortImage.height();
    
    leftDisparityImage_.resize(width, height, 1);
    rightDisparityImage_.resize(width, height, 1);
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            leftDisparityImage_(x, y) = static_cast<float>(leftDisparityShortImage(x, y))/disparityFactor;
            rightDisparityImage_(x, y) = static_cast<float>(rightDisparityShortImage(x, y))/disparityFactor;
        }
    }
}

void StereoSlic::setGridSeedPoint(const int superpixelTotal) {
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    int imageSize = width*height;
    
    gridSize_ = sqrt(static_cast<double>(imageSize)/superpixelTotal);
    stepSize_ = static_cast<int>(gridSize_ + 2.0);
    int offsetX = static_cast<int>(gridSize_/2.0);
    int offsetY = static_cast<int>(gridSize_/2.0);
    
    seeds_.clear();
    for (int indexY = 0; indexY < height; ++indexY) {
        int y = static_cast<int>(gridSize_*indexY + offsetY + 0.5);
        if (y >= height) break;
        for (int indexX = 0; indexX < width; ++indexX) {
            int x = static_cast<int>(gridSize_*indexX + offsetX + 0.5);
            if (x >= width) break;
            
            LabXYD newSeed;
            newSeed.color[0] = leftLabImage_(x, y, 0);
            newSeed.color[1] = leftLabImage_(x, y, 1);
            newSeed.color[2] = leftLabImage_(x, y, 2);
            newSeed.position[0] = x;
            newSeed.position[1] = y;
            seeds_.push_back(newSeed);
        }
    }
}

void StereoSlic::perturbSeeds() {
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    
    int seedTotal = static_cast<int>(seeds_.size());
    for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
        int originalX = seeds_[seedIndex].position[0];
        int originalY = seeds_[seedIndex].position[1];
        int originalPixelIndex = width*originalY + originalX;
        
        int perturbedPixelIndex = originalPixelIndex;
        for (int neighborIndex = 0; neighborIndex < eightNeighborTotal; ++neighborIndex) {
            int neighborX = originalX + eightNeighborOffsetX[neighborIndex];
            int neighborY = originalY + eightNeighborOffsetY[neighborIndex];
            if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;
            int neighborPixelIndex = width*neighborY + neighborX;
            
            if (leftLabEdges_[neighborPixelIndex] < leftLabEdges_[perturbedPixelIndex]) {
                perturbedPixelIndex = neighborPixelIndex;
            }
        }
        
        if (perturbedPixelIndex != originalPixelIndex) {
            int perturbedX = perturbedPixelIndex%width;
            int perturbedY = perturbedPixelIndex/width;
            
            seeds_[seedIndex].color[0] = leftLabImage_(perturbedX, perturbedY, 0);
            seeds_[seedIndex].color[1] = leftLabImage_(perturbedX, perturbedY, 1);
            seeds_[seedIndex].color[2] = leftLabImage_(perturbedX, perturbedY, 2);
            seeds_[seedIndex].position[0] = perturbedX;
            seeds_[seedIndex].position[1] = perturbedY;
        }
    }
}

void StereoSlic::assignLabel() {
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    int seedTotal = static_cast<int>(seeds_.size());
    
    double positionWeight = compactnessWeight_/(stepSize_*stepSize_);
    
    labels_.resize(width*height, -1);
    std::vector<double> distancesToSeeds(width*height, DBL_MAX);
    for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
        int minX = (seeds_[seedIndex].position[0] > stepSize_) ? seeds_[seedIndex].position[0] - stepSize_ : 0;
        int maxX = (seeds_[seedIndex].position[0] < width - stepSize_) ? seeds_[seedIndex].position[0] + stepSize_ : width;
        int minY = (seeds_[seedIndex].position[1] > stepSize_) ? seeds_[seedIndex].position[1] - stepSize_ : 0;
        int maxY = (seeds_[seedIndex].position[1] < height - stepSize_) ? seeds_[seedIndex].position[1] + stepSize_ : height;
        
        double seedL = seeds_[seedIndex].color[0];
        double seedA = seeds_[seedIndex].color[1];
        double seedB = seeds_[seedIndex].color[2];
        double seedX = seeds_[seedIndex].position[0];
        double seedY = seeds_[seedIndex].position[1];
        double seedAlpha = seeds_[seedIndex].disparityPlane[0];
        double seedBeta = seeds_[seedIndex].disparityPlane[1];
        double seedGamma = seeds_[seedIndex].disparityPlane[2];
        
        for (int y = minY; y < maxY; ++y) {
            for (int x = minX; x < maxX; ++x) {
                double distanceLeftLab = (leftLabImage_(x, y, 0) - seedL)*(leftLabImage_(x, y, 0) - seedL)
                                            + (leftLabImage_(x, y, 1) - seedA)*(leftLabImage_(x, y, 1) - seedA)
                                            + (leftLabImage_(x, y, 2) - seedB)*(leftLabImage_(x, y, 2) - seedB);
                double distanceXY = (x - seedX)*(x - seedX) + (y - seedY)*(y - seedY);
                
                // Default distances
                double distanceRightLab = distanceLeftLab;
                double distanceLeftD = noDisparityPenalty_;
                double distanceRightD = noDisparityPenalty_;
                
                if (energyType_ > 0) {
                    double estimatedDisparity = seedAlpha*x + seedBeta*y + seedGamma;
                    if (estimatedDisparity > 0) {
                        if (leftDisparityImage_(x, y) > 0) {
                            // distance of left disparities
                            distanceLeftD = (leftDisparityImage_(x, y) - estimatedDisparity)*(leftDisparityImage_(x, y) - estimatedDisparity);
                        }
                        
                        if (energyType_ > 1) {
                            int rightX = static_cast<int>(x - estimatedDisparity + 0.5);
                            if (rightX >= 0 && rightDisparityImage_(rightX, y) > 0) {
                                double rightDisparityDifference = estimatedDisparity - rightDisparityImage_(rightX, y);
                                if (rightDisparityDifference < -1) {
                                    // Occluded
                                    rightDisparityDifference = 0;
                                    distanceRightLab = distanceLeftLab;
                                } else {
                                    // Non-occluded
                                    distanceRightD = rightDisparityDifference*rightDisparityDifference;
                                    if (energyType_ > 2) {
                                        distanceRightLab = (rightLabImage_(rightX, y, 0) - seedL)*(rightLabImage_(rightX, y, 0) - seedL)
                                                            + (rightLabImage_(rightX, y, 1) - seedA)*(rightLabImage_(rightX, y, 1) - seedA)
                                                            + (rightLabImage_(rightX, y, 2) - seedB)*(rightLabImage_(rightX, y, 2) - seedB);
                                    }
                                }
                            }
                        }
                    }
                }
                
                double distanceLab = distanceLeftLab + distanceRightLab;
                double distanceD = distanceLeftD + distanceRightD;
                
                double distanceLabXYD = distanceLab + positionWeight*distanceXY + disparityWeight_*distanceD;
                if (distanceLabXYD < distancesToSeeds[width*y + x]) {
                    distancesToSeeds[width*y + x] = distanceLabXYD;
                    labels_[width*y + x] = seedIndex;
                }
            }
        }
    }
}

void StereoSlic::updateSeeds() {
    updateSeedsColorAndPosition();
    if (energyType_ > 0) {
        estimateDisparityPlaneParameter();
    }
}

void StereoSlic::updateSeedsColorAndPosition() {
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    int seedTotal = static_cast<int>(seeds_.size());
    
    std::vector<int> segmentSizes(seedTotal, 0);
    std::vector<LabXYD> segmentSigmas(seedTotal);
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            int pixelSeedIndex = labels_[width*y + x];
            if (pixelSeedIndex >= 0) {
                segmentSigmas[pixelSeedIndex].color[0] += leftLabImage_(x, y, 0);
                segmentSigmas[pixelSeedIndex].color[1] += leftLabImage_(x, y, 1);
                segmentSigmas[pixelSeedIndex].color[2] += leftLabImage_(x, y, 2);
                segmentSigmas[pixelSeedIndex].position[0] += x;
                segmentSigmas[pixelSeedIndex].position[1] += y;
                ++segmentSizes[pixelSeedIndex];
            }
        }
    }
    
    for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
        double segmentSizeInverse = 1.0;
        if (segmentSizes[seedIndex] > 0) segmentSizeInverse = 1.0/segmentSizes[seedIndex];
        
        seeds_[seedIndex].color[0] = segmentSigmas[seedIndex].color[0]*segmentSizeInverse;
        seeds_[seedIndex].color[1] = segmentSigmas[seedIndex].color[1]*segmentSizeInverse;
        seeds_[seedIndex].color[2] = segmentSigmas[seedIndex].color[2]*segmentSizeInverse;
        seeds_[seedIndex].position[0] = segmentSigmas[seedIndex].position[0]*segmentSizeInverse;
        seeds_[seedIndex].position[1] = segmentSigmas[seedIndex].position[1]*segmentSizeInverse;
    }
}

void StereoSlic::estimateDisparityPlaneParameter() {
    std::vector< std::vector<DisparityPixel> > segmentDisparityPixels = makeDisparityPixelList();
    int seedTotal = static_cast<int>(segmentDisparityPixels.size());
    
    for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
        std::vector<double> planeParameter = estimateDisparityPlaneParameter(segmentDisparityPixels[seedIndex]);
        
        seeds_[seedIndex].disparityPlane[0] = planeParameter[0];
        seeds_[seedIndex].disparityPlane[1] = planeParameter[1];
        seeds_[seedIndex].disparityPlane[2] = planeParameter[2];
    }
}

std::vector< std::vector<StereoSlic::DisparityPixel> > StereoSlic::makeDisparityPixelList() const {
    int width = leftDisparityImage_.width();
    int height = leftDisparityImage_.height();
    int seedTotal = static_cast<int>(seeds_.size());
    
    std::vector< std::vector<DisparityPixel> > segmentDisparityPixels(seedTotal);
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            if (leftDisparityImage_(x, y) > 0) {
                int pixelSeedIndex = labels_[width*y + x];
                if (pixelSeedIndex >= 0) {
                    DisparityPixel newDisparityPixel;
                    newDisparityPixel.x = x;
                    newDisparityPixel.y = y;
                    newDisparityPixel.d = leftDisparityImage_(x, y);
                    segmentDisparityPixels[pixelSeedIndex].push_back(newDisparityPixel);
                }
            }
        }
    }
    
    return segmentDisparityPixels;
}

std::vector<double> StereoSlic::estimateDisparityPlaneParameter(const std::vector<DisparityPixel>& disparityPixels) const {
    std::vector<double> planeParameter(3);
    
    int pixelTotal = static_cast<int>(disparityPixels.size());
    if (pixelTotal < 3) {
        planeParameter[0] = 0;
        planeParameter[1] = 0;
        planeParameter[2] = -1;
    } else {
        bool isSameX = true;
        bool isSameY = true;
        for (int pixelIndex = 1; pixelIndex < pixelTotal; ++pixelIndex) {
            if (disparityPixels[pixelIndex].x != disparityPixels[0].x) isSameX = false;
            if (disparityPixels[pixelIndex].y != disparityPixels[0].y) isSameY = false;
            if (!isSameX && !isSameY) break;
        }
        if (isSameX || isSameY) {
            double disparitySum = 0.0;
            for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) disparitySum += disparityPixels[pixelIndex].d;
            
            planeParameter[0] = 0;
            planeParameter[1] = 0;
            planeParameter[2] = -1;
        } else {
            planeParameter = estimateDisparityPlaneParameterRANSAC(disparityPixels);
        }
    }
    
    return planeParameter;
}

std::vector<double> StereoSlic::estimateDisparityPlaneParameterRANSAC(const std::vector<DisparityPixel>& disparityPixels) const {
    const double inlierThreshold = 2.0;
    const double confidenceLevel = 0.99;
    
    int pixelTotal = static_cast<int>(disparityPixels.size());
    
    int samplingTotal = pixelTotal*2;
    
    int bestInlierTotal = 0;
    std::vector<bool> bestInlierFlags(pixelTotal);
    int samplingCount = 0;
    while (samplingCount < samplingTotal) {
        // Randomly select 3 pixels
        int drawIndices[3];
        drawIndices[0] = rand()%pixelTotal;
        drawIndices[1] = rand()%pixelTotal;
        while(drawIndices[1] == drawIndices[0]) drawIndices[1] = rand()%pixelTotal;
        drawIndices[2] = rand()%pixelTotal;
        while(drawIndices[2] == drawIndices[1] || drawIndices[2] == drawIndices[0]
              || (disparityPixels[drawIndices[0]].x == disparityPixels[drawIndices[1]].x && disparityPixels[drawIndices[0]].x == disparityPixels[drawIndices[2]].x)
              || (disparityPixels[drawIndices[0]].y == disparityPixels[drawIndices[1]].y && disparityPixels[drawIndices[0]].y == disparityPixels[drawIndices[2]].y))
        {
            drawIndices[2] = rand()%pixelTotal;
        }
        
        // Compute plane parameters
        std::vector<double> planeParameter;
        solvePlaneEquations(disparityPixels[drawIndices[0]].x, disparityPixels[drawIndices[0]].y, 1, disparityPixels[drawIndices[0]].d,
                            disparityPixels[drawIndices[1]].x, disparityPixels[drawIndices[1]].y, 1, disparityPixels[drawIndices[1]].d,
                            disparityPixels[drawIndices[2]].x, disparityPixels[drawIndices[2]].y, 1, disparityPixels[drawIndices[2]].d,
                            planeParameter);
        
        // Count the number of inliers
        int inlierTotal = 0;
        std::vector<bool> inlierFlags(pixelTotal);
        for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
            double estimatedDisparity = planeParameter[0]*disparityPixels[pixelIndex].x
                                        + planeParameter[1]*disparityPixels[pixelIndex].y
                                        + planeParameter[2];
            if (fabs(estimatedDisparity - disparityPixels[pixelIndex].d) <= inlierThreshold) {
                ++inlierTotal;
                inlierFlags[pixelIndex] = true;
            } else {
                inlierFlags[pixelIndex] = false;
            }
        }
        
        // Update best inliers
        if (inlierTotal > bestInlierTotal) {
            bestInlierTotal = inlierTotal;
            bestInlierFlags = inlierFlags;
            
            samplingTotal = computeRequiredSamplingTotal(3, bestInlierTotal, pixelTotal, samplingTotal, confidenceLevel);
        }
        
        // Increment
        ++samplingCount;
    }
    
    // Least-square estimation
    double sumXSqr = 0, sumYSqr = 0, sumXY = 0, sumX = 0, sumY = 0;
    double sumXD = 0, sumYD = 0, sumD = 0;
    int inlierIndex = 0;
    for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
        if (bestInlierFlags[pixelIndex]) {
            sumXSqr += disparityPixels[pixelIndex].x*disparityPixels[pixelIndex].x;
            sumYSqr += disparityPixels[pixelIndex].y*disparityPixels[pixelIndex].y;
            sumXY += disparityPixels[pixelIndex].x*disparityPixels[pixelIndex].y;
            sumX += disparityPixels[pixelIndex].x;
            sumY += disparityPixels[pixelIndex].y;
            sumXD += disparityPixels[pixelIndex].x*disparityPixels[pixelIndex].d;
            sumYD += disparityPixels[pixelIndex].y*disparityPixels[pixelIndex].d;
            sumD += disparityPixels[pixelIndex].d;
            ++inlierIndex;
        }
    }
    std::vector<double> planeParameter(3);
    solvePlaneEquations(sumXSqr, sumXY, sumX, sumXD,
                        sumXY, sumYSqr, sumY, sumYD,
                        sumX, sumY, inlierIndex, sumD,
                        planeParameter);
    
    return planeParameter;
}

void StereoSlic::solvePlaneEquations(const double x1, const double y1, const double z1, const double d1,
                                     const double x2, const double y2, const double z2, const double d2,
                                     const double x3, const double y3, const double z3, const double d3,
                                     std::vector<double>& planeParameter) const
{
    const double epsilonValue = 1e-10;
    
    planeParameter.resize(3);
    
    double denominatorA = (x1*z2 - x2*z1)*(y2*z3 - y3*z2) - (x2*z3 - x3*z2)*(y1*z2 - y2*z1);
    if (denominatorA < epsilonValue) {
        planeParameter[0] = 0.0;
        planeParameter[1] = 0.0;
        planeParameter[2] = -1.0;
        return;
    }
    
    planeParameter[0] = ((z2*d1 - z1*d2)*(y2*z3 - y3*z2) - (z3*d2 - z2*d3)*(y1*z2 - y2*z1))/denominatorA;

    double denominatorB = y1*z2 - y2*z1;
    if (denominatorB > epsilonValue) {
        planeParameter[1] = (z2*d1 - z1*d2 - planeParameter[0]*(x1*z2 - x2*z1))/denominatorB;
    } else {
        denominatorB = y2*z3 - y3*z2;
        planeParameter[1] = (z3*d2 - z2*d3 - planeParameter[0]*(x2*z3 - x3*z2))/denominatorB;
    }
    if (z1 > epsilonValue) {
        planeParameter[2] = (d1 - planeParameter[0]*x1 - planeParameter[1]*y1)/z1;
    } else if (z2 > epsilonValue) {
        planeParameter[2] = (d2 - planeParameter[0]*x2 - planeParameter[1]*y2)/z2;
    } else {
        planeParameter[2] = (d3 - planeParameter[0]*x3 - planeParameter[1]*y3)/z3;
    }
}

void StereoSlic::labelConnectedPixels(const int x, const int y, const int newLabelIndex,
                                      std::vector<int>& newLabels, std::vector<int>& connectedXs, std::vector<int>& connectedYs) const
{
    int width = leftLabImage_.width();
    int height = leftLabImage_.height();
    
    int originalLabelIndex = labels_[width*y + x];
    for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
        int neighborX = x + fourNeighborOffsetX[neighborIndex];
        int neighborY = y + fourNeighborOffsetY[neighborIndex];
        if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;
        
        if (newLabels[width*neighborY + neighborX] < 0 && labels_[width*neighborY + neighborX] == originalLabelIndex) {
            connectedXs.push_back(neighborX);
            connectedYs.push_back(neighborY);
            newLabels[width*neighborY + neighborX] = newLabelIndex;
            labelConnectedPixels(neighborX, neighborY, newLabelIndex, newLabels, connectedXs, connectedYs);
        }
    }
}


int computeRequiredSamplingTotal(const int drawTotal, const int inlierTotal, const int pointTotal, const int currentSamplingTotal, const double confidenceLevel) {
    double ep = 1 - static_cast<double>(inlierTotal)/static_cast<double>(pointTotal);
    if (ep == 1.0) {
        ep = 0.5;
    }
    
    int newSamplingTotal = static_cast<int>(log(1 - confidenceLevel)/log(1 - pow(1 - ep, drawTotal)) + 0.5);
    if (newSamplingTotal < currentSamplingTotal) {
        return newSamplingTotal;
    } else {
        return currentSamplingTotal;
    }
}
