#include <iostream>
#include <string>
#include <time.h>
#include <cmdline.h>
#include <revlib.h>
#include "StereoSlic.h"

struct ParameterStereoSlic {
    bool verbose;
    double disparityFactor;
    int energyType;   // 0: color(Left), 1:color(Left)+disparity(Left),
                      // 2: color(Left)+disparity(Left,Right), 3: color(Left,Right)+disparity(Left,Right)
    int superpixelTotal;
    int iterationTotal;
    double compactnessWeight;
    double disparityWeight;
    double noDisparityPenalty;
    std::string leftImageFilename;
    std::string rightImageFilename;
    std::string leftDisparityImageFilename;
    std::string rightDisparityImageFilename;
    std::string outputSegmentImageFilename;
    std::string outputBoundaryImageFilename;
    std::string outputDisparityImageFilename;
};

// Prototype declaration
cmdline::parser makeCommandParser();
ParameterStereoSlic parseCommandline(int argc, char* argv[]);

cmdline::parser makeCommandParser() {
    cmdline::parser commandParser;
    commandParser.add<std::string>("output", 'o', "output file", false, "");
    commandParser.add<double>("factor", 'f', "disparity factor of input disparity image", false, 256.0);
    commandParser.add<int>("type", 't', "energy type (0:colorL, 1:colorL+disparityL, 2:colorL+disparityLR, 3:colorLR+disparityLR", false, 3);
    commandParser.add<int>("superpixel", 's', "the number of superpixels", false, 1000);
    commandParser.add<int>("iteration", 'i', "the number of iterations", false, 10);
    commandParser.add<double>("compactness", 'c', "compactness weight", false, 3000);
    commandParser.add<double>("disparity", 'd', "weight of disparity term", false, 30);
    commandParser.add<double>("penalty", 'p', "penalty value of disparity term without disparity estimation", false, 3);
    commandParser.add("verbose", 'v', "verbose");
    commandParser.add("help", 'h', "display this message");
    commandParser.footer("left_image right_image left_disparity_image right_disparity_image");
    commandParser.set_program_name("stereoSlic");
    
    return commandParser;
}

ParameterStereoSlic parseCommandline(int argc, char* argv[]) {
    // Make command parser
    cmdline::parser commandParser = makeCommandParser();
    // Parse command line
    bool isCorrectCommandline = commandParser.parse(argc, argv);
    // Check arguments
    if (!isCorrectCommandline) {
        std::cerr << commandParser.error() << std::endl;
    }
    if (!isCorrectCommandline || commandParser.exist("help") || commandParser.rest().size() < 4) {
        std::cerr << commandParser.usage() << std::endl;
        exit(1);
    }
    
    // Set program parameters
    ParameterStereoSlic parameters;
    // Verbose flag
    parameters.verbose = commandParser.exist("verbose");
    // Disparity factor
    parameters.disparityFactor = commandParser.get<double>("factor");
    // Energy type
    parameters.energyType = commandParser.get<int>("type");
    // The number of superpixels
    parameters.superpixelTotal = commandParser.get<int>("superpixel");
    // The number of iterations
    parameters.iterationTotal = commandParser.get<int>("iteration");
    // Compactness factor
    parameters.compactnessWeight = commandParser.get<double>("compactness");
    // Disparity weight
    parameters.disparityWeight = commandParser.get<double>("disparity");
    // Occluded penalty
    parameters.noDisparityPenalty = commandParser.get<double>("penalty");
    // Input files
    parameters.leftImageFilename = commandParser.rest()[0];
    parameters.rightImageFilename = commandParser.rest()[1];
    parameters.leftDisparityImageFilename = commandParser.rest()[2];
    parameters.rightDisparityImageFilename = commandParser.rest()[3];
    // Output files
    std::string outputSegmentImageFilename = commandParser.get<std::string>("output");
    if (outputSegmentImageFilename == "") {
        outputSegmentImageFilename = parameters.leftImageFilename;
        size_t dotPosition = outputSegmentImageFilename.rfind('.');
        if (dotPosition != std::string::npos) outputSegmentImageFilename.erase(dotPosition);
        outputSegmentImageFilename += "_slic.png";
    }
    parameters.outputSegmentImageFilename = outputSegmentImageFilename;
    std::string outputBoundaryImageFilename = outputSegmentImageFilename;
    size_t dotPosition = outputBoundaryImageFilename.rfind('.');
    if (dotPosition != std::string::npos) outputBoundaryImageFilename.erase(dotPosition);
    std::string outputDisparityImageFilename = outputBoundaryImageFilename;
    outputBoundaryImageFilename += "_boundary.png";
    outputDisparityImageFilename += "_disparity.png";
    parameters.outputBoundaryImageFilename = outputBoundaryImageFilename;
    parameters.outputDisparityImageFilename = outputDisparityImageFilename;
    
    return parameters;
}

int main(int argc, char* argv[]) {
    // Parse command line
    ParameterStereoSlic parameters = parseCommandline(argc, argv);
    
    // Output parameters
    if (parameters.verbose) {
        std::cerr << std::endl;
        std::cerr << "Left image:      " << parameters.leftImageFilename << std::endl;
        std::cerr << "Right image:     " << parameters.rightImageFilename << std::endl;
        std::cerr << "Left disparity:  " << parameters.leftDisparityImageFilename << std::endl;
        std::cerr << "Right disparity: " << parameters.rightDisparityImageFilename << std::endl;
        std::cerr << "Output segment image:   " << parameters.outputSegmentImageFilename << std::endl;
        std::cerr << "Output boundary image:  " << parameters.outputBoundaryImageFilename << std::endl;
        std::cerr << "Output disparity image: " << parameters.outputDisparityImageFilename << std::endl;
        std::cerr << "   disparity factor:     " << parameters.disparityFactor << std::endl;
        std::cerr << "   energy type:          ";
        if (parameters.energyType == 0) std::cerr << "color(left)" << std::endl;
        else if (parameters.energyType == 1) std::cerr << "color(left) + disparity(left)" << std::endl;
        else if (parameters.energyType == 2) std::cerr << "color(left) + disparity(left+right)" << std::endl;
        else std::cerr << "color(left+right) + disparity(left+right)" << std::endl;
        std::cerr << "   #superpixels:         " << parameters.superpixelTotal << std::endl;
        std::cerr << "   #iterations:          " << parameters.iterationTotal << std::endl;
        std::cerr << "   compactness weight:   " << parameters.compactnessWeight << std::endl;
        std::cerr << "   disparity weight:     " << parameters.disparityWeight << std::endl;
        std::cerr << "   no disparity penalty: " << parameters.noDisparityPenalty << std::endl;
        std::cerr << std::endl;
    }
    
    try {
        // Open input image
        rev::Image<unsigned char> leftImage = rev::readImageFile(parameters.leftImageFilename);
        rev::Image<unsigned char> rightImage = rev::readImageFile(parameters.rightImageFilename);
        rev::Image<unsigned short> leftDisparityImage = rev::read16bitImageFile(parameters.leftDisparityImageFilename);
        rev::Image<unsigned short> rightDisparityImage = rev::read16bitImageFile(parameters.rightDisparityImageFilename);
        
        clock_t startClock, endClock;
        startClock = clock();

        // StereoSLIC
        StereoSlic stereoSlic;
        stereoSlic.setEnergyType(parameters.energyType);
        stereoSlic.setIterationTotal(parameters.iterationTotal);
        stereoSlic.setWeightParameters(parameters.compactnessWeight, parameters.disparityWeight, parameters.noDisparityPenalty);
        // Perform superpixel segmentation
        rev::Image<unsigned short> segmentImage;
        rev::Image<unsigned short> segmentDisparityImage;
        stereoSlic.segment(parameters.superpixelTotal,
                           leftImage, rightImage,
                           leftDisparityImage, rightDisparityImage,
                           parameters.disparityFactor,
                           segmentImage,
                           segmentDisparityImage);
        
        endClock = clock();
        if (parameters.verbose) {
            std::cerr << "Computation time: " << static_cast<double>(endClock - startClock)/CLOCKS_PER_SEC << " sec." << std::endl;
            std::cerr << std::endl;
        }
        
        // Output
        rev::write16bitImageFile(parameters.outputSegmentImageFilename, segmentImage);
        rev::write16bitImageFile(parameters.outputDisparityImageFilename, segmentDisparityImage);
        rev::Image<unsigned char> boundaryImage = rev::drawSegmentBoundary(leftImage, segmentImage);
        rev::writeImageFile(parameters.outputBoundaryImageFilename, boundaryImage);
        
    } catch (const rev::Exception& revException) {
        std::cerr << "Error [" << revException.functionName() << "]: " << revException.message() << std::endl;
        exit(1);
    }
}
