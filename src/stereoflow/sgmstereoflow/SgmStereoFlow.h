#pragma once

#include <revlib.h>
#include "CameraMotion.h"
#include "CalibratedEpipolarFlowGeometry.h"

class SgmStereoFlow {
public:
	SgmStereoFlow();

	void setDisparityTotal(const int disparityTotal);
	void setDataCostParameters(const int dataCostType,
		const int sobelCapValue,
		const int censusWindowRadius,
		const int censusWeightFactor,
		const int aggregationWindowRadius);
	void setSmoothnessCostParameters(const int smoothnessPenaltySmall, const int smoothnessPenaltyLarge);
	void setSgmParameters(const bool pathEightDirections, const int consistencyThreshold);

	void compute(const rev::Image<unsigned char>& firstLeftImage,
		const rev::Image<unsigned char>& firstRightImage,
		const rev::Image<unsigned char>& secondLeftImage,
		const rev::Image<unsigned short>& stereoDisparityImage,
		const rev::Image<unsigned short>& flowVzIndexImage,
		const CameraMotion& cameraMotion,
		rev::Image<unsigned short>& leftDisparityImage,
		rev::Image<unsigned short>& firstFlowImage);

private:
	void allocateBuffer(const int width, const int height);
	void makeIntensityDifferenceImage(const rev::Image<unsigned char>& leftImage);
	rev::Image<unsigned short> performSGM(const rev::Image<unsigned short>& costImage);
	void enforceLeftRightConsistency(rev::Image<unsigned short>& leftDisparityImage,
		rev::Image<unsigned short>& rightDisparityImage,
		rev::Image<unsigned short>& secondLeftDisparityImage,
		const rev::Image<unsigned short>& stereoDisparityImage,
		const rev::Image<unsigned short>& flowVzIndexImage,
		const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarGeometry) const;
	void computeFlowImage(const rev::Image<unsigned short>& disparityImage,
		const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarGeometry,
		rev::Image<unsigned short>& flowImage) const;
	void computeVzIndexFlowImage(const rev::Image<unsigned short>& disparityImage,
		const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarGeometry,
		rev::Image<unsigned short>& firstVzIndexImage,
		rev::Image<unsigned short>& secondVzIndexImage,
		rev::Image<unsigned short>& flowImage) const;
	void convertVZIndexImageFromFirstToSecond(const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		const rev::Image<unsigned short>& firstVZIndexImage,
		rev::Image<unsigned short>& secondVZIndexImage) const;

	void setBufferSize(const int width, const int height);
	void speckleFilter(const int maxSpeckleSize, const int maxDifference,
		rev::Image<unsigned short>& image) const;

	// Parameter
	int disparityTotal_;
	int dataCostType_;
	int sobelCapValue_;
	int censusWindowRadius_;
	double censusWeightFactor_;
	int aggregationWindowRadius_;
	int smoothnessPenaltySmall_;
	int smoothnessPenaltyLarge_;
	bool pathEightDirections_;
	int consistencyThreshold_;

	// Data
	int pathRowBufferTotal_;
	int disparitySize_;
	int pathTotal_;
	int pathDisparitySize_;
	int costSumBufferRowSize_;
	int costSumBufferSize_;
	int pathMinCostBufferSize_;
	int pathCostBufferSize_;
	int totalBufferSize_;
	rev::AlignedMemory<short> buffer_;
	rev::Image<unsigned char> intensityDifferenceImage_;
};
