#include <iostream>
#include <string>
#include <time.h>
#include <cmdline.h>
#include <revlib.h>
#include "CameraMotion.h"
#include "SgmStereoFlow.h"

struct ParameterSgmStereoFlow {
	bool verbose;
	int disparityTotal;
	int smallPenalty;
	int largePenalty;
	std::string firstLeftImageFilename;
	std::string firstRightImageFilename;
	std::string secondLeftImageFilename;
	std::string stereoDisparityImageFilename;
	std::string flowVzIndexImageFilename;
	std::string cameraMotionFilename;
	std::string outputLeftDisparityImageFilename;
	std::string outputFirstFlowImageFilename;
};

// Prototype declaration
ParameterSgmStereoFlow parseCommandline(int argc, char* argv[]);

ParameterSgmStereoFlow parseCommandline(int argc, char* argv[]) {
	// Make command parser
	cmdline::parser commandParser;
	commandParser.add<std::string>("output", 'o', "output directory", false, "");
	commandParser.add<int>("disparity", 'd', "number of disparities", false, 256);
	commandParser.add<int>("small", 's', "small penalty", false, 4);
	commandParser.add<int>("large", 'l', "large penalty", false, 64);
	commandParser.add("verbose", 'v', "verbose");
	commandParser.add("help", 'h', "display this message");
	commandParser.set_program_name("sgmstereoflow");
	commandParser.footer("first_left_image first_right_image second_left_image stereo_disparity_image flow_vzindex_image camera_motion");

	// Parse command line
	bool isCorrectCommand = commandParser.parse(argc, argv);
	if (!isCorrectCommand) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommand || commandParser.exist("help") || commandParser.rest().size() < 6) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	// Set program parameters
	ParameterSgmStereoFlow parameters;
	// Verbose flag
	parameters.verbose = commandParser.exist("verbose");
	// The number of disparities
	parameters.disparityTotal = commandParser.get<int>("disparity");
	// Penalty value
	parameters.smallPenalty = commandParser.get<int>("small");
	parameters.largePenalty = commandParser.get<int>("large");
	// Input stereo images
	parameters.firstLeftImageFilename = commandParser.rest()[0];
	parameters.firstRightImageFilename = commandParser.rest()[1];
	parameters.secondLeftImageFilename = commandParser.rest()[2];
	parameters.stereoDisparityImageFilename = commandParser.rest()[3];
	parameters.flowVzIndexImageFilename = commandParser.rest()[4];
	parameters.cameraMotionFilename = commandParser.rest()[5];
	// Output directory
	std::string outputDirectoryname = commandParser.get<std::string>("output");
	if (outputDirectoryname == "") {
		// Same directory as input
		// Output left disparity image
		std::string outputLeftBaseFilename = parameters.firstLeftImageFilename;
		size_t dotPosition = outputLeftBaseFilename.rfind('.');
		if (dotPosition != std::string::npos) outputLeftBaseFilename.erase(dotPosition);
		std::string outputLeftDisparityImageFilename = outputLeftBaseFilename + "_left_disparity.png";
		std::string outputFirstFlowImageFilename = outputLeftBaseFilename + "_flow.png";
		parameters.outputLeftDisparityImageFilename = outputLeftDisparityImageFilename;
		parameters.outputFirstFlowImageFilename = outputFirstFlowImageFilename;
	} else {
		// Output directory is specified
		// Output left disparity image
		std::string outputLeftBaseFilename = parameters.firstLeftImageFilename;
		size_t slashPosition = outputLeftBaseFilename.rfind('/');
		if (slashPosition != std::string::npos) outputLeftBaseFilename.erase(0, slashPosition+1);
		else {
			slashPosition = outputLeftBaseFilename.rfind('\\');
			if (slashPosition != std::string::npos) outputLeftBaseFilename.erase(0, slashPosition+1);
		}
		size_t dotPosition = outputLeftBaseFilename.rfind('.');
		if (dotPosition != std::string::npos) outputLeftBaseFilename.erase(dotPosition);
		std::string outputLeftDisparityImageFilename = outputDirectoryname + "/" + outputLeftBaseFilename + "_left_disparity.png";
		std::string outputFirstFlowImageFilename = outputDirectoryname + "/" + outputLeftBaseFilename + "_flow.png";
		parameters.outputLeftDisparityImageFilename = outputLeftDisparityImageFilename;
		parameters.outputFirstFlowImageFilename = outputFirstFlowImageFilename;
	}

	return parameters;
}

int main(int argc, char* argv[]) {
	// Parse command line
	ParameterSgmStereoFlow parameters = parseCommandline(argc, argv);

	// Output parameters
	if (parameters.verbose) {
		std::cerr << std::endl;
		std::cerr << "First left image:       " << parameters.firstLeftImageFilename << std::endl;
		std::cerr << "First right image:      " << parameters.firstRightImageFilename << std::endl;
		std::cerr << "Second left image:      " << parameters.secondLeftImageFilename << std::endl;
		std::cerr << "Camera motion:          " << parameters.cameraMotionFilename << std::endl;
		std::cerr << "Output left disparity:  " << parameters.outputLeftDisparityImageFilename << std::endl;
		std::cerr << "Output flow:            " << parameters.outputFirstFlowImageFilename << std::endl;
		std::cerr << "   #disparities:     " << parameters.disparityTotal << std::endl;
		std::cerr << std::endl;
	}

	try {
		rev::Image<unsigned char> firstLeftImage = rev::readImageFile(parameters.firstLeftImageFilename);
		rev::Image<unsigned char> firstRightImage = rev::readImageFile(parameters.firstRightImageFilename);
		rev::Image<unsigned char> secondLeftImage = rev::readImageFile(parameters.secondLeftImageFilename);
		rev::Image<unsigned short> stereoDisparityImage = rev::read16bitImageFile(parameters.stereoDisparityImageFilename);
		rev::Image<unsigned short> flowVzIndexImage = rev::read16bitImageFile(parameters.flowVzIndexImageFilename);
		CameraMotion cameraMotion;
		cameraMotion.readCameraMotionFile(parameters.cameraMotionFilename);

		// SGM-StereoFlow
		SgmStereoFlow sgmStereoFlow;
		sgmStereoFlow.setDisparityTotal(parameters.disparityTotal);

		// Compute disparity images
		clock_t startClock, endClock;
		rev::Image<unsigned short> leftDisparityImage;
		rev::Image<unsigned short> firstFlowImage;
		startClock = clock();
		sgmStereoFlow.compute(firstLeftImage, firstRightImage, secondLeftImage, stereoDisparityImage, flowVzIndexImage, cameraMotion,
			leftDisparityImage, firstFlowImage);
		endClock = clock();
		if (parameters.verbose)	std::cout << "Time: " << static_cast<double>(endClock - startClock)/CLOCKS_PER_SEC << std::endl;

		// Write disparity images
		rev::write16bitImageFile(parameters.outputLeftDisparityImageFilename, leftDisparityImage);
		rev::write16bitImageFile(parameters.outputFirstFlowImageFilename, firstFlowImage);

	} catch (const rev::Exception& revException) {
		std::cerr << "Error [" << revException.functionName() << "]: " << revException.message() << std::endl;
		exit(1);
	}
}
