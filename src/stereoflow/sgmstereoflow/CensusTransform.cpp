#include "CensusTransform.h"

const int CENSUSTRANSFORM_DEFAUT_WINDOW_RADIUS = 2;
const int CENSUSTRANSFORM_DEFAULT_HORIZONTAL_RADIUS = CENSUSTRANSFORM_DEFAUT_WINDOW_RADIUS;
const int CENSUSTRANSFORM_DEFAULT_VERTICAL_RADIUS = CENSUSTRANSFORM_DEFAUT_WINDOW_RADIUS;

CensusTransform::CensusTransform() : horizontalRadius_(CENSUSTRANSFORM_DEFAULT_HORIZONTAL_RADIUS),
	verticalRadius_(CENSUSTRANSFORM_DEFAULT_VERTICAL_RADIUS) {}

void CensusTransform::setWindowRadius(const int windowRadius) {
	if (windowRadius < 1 || windowRadius > 2) {
		throw rev::Exception("CensusTransform::setWindowRadius", "window radius must be 1 or 2");
	}

	horizontalRadius_ = windowRadius;
	verticalRadius_ = windowRadius;
}

void CensusTransform::setWindowRadius(const int horizontalRadius, const int verticalRadius) {
	int windowSize = (2*horizontalRadius + 1)*(2*verticalRadius + 1);
	if (windowSize <= 1 || windowSize > 32) {
		throw rev::Exception("CensusTransform::setWindowRadius", "acceptable range of window size is 2-32");
	}

	horizontalRadius_ = horizontalRadius;
	verticalRadius_ = verticalRadius;
}

void CensusTransform::transform(const rev::Image<unsigned char>& grayscaleImage,
								rev::Image<int>& censusImage) const
{
	int windowSize = (2*horizontalRadius_ + 1)*(2*verticalRadius_ + 1);
	if (windowSize > 32) {
		throw rev::Exception("CensusTransform::transform", "transform64bit should be used");
	}
	if (grayscaleImage.channelTotal() != 1) {
		throw rev::Exception("CensusTransform::transform", "input image must be a grayscale image");
	}

	int width = grayscaleImage.width();
	int height = grayscaleImage.height();

	censusImage.resize(width, height, 1);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			unsigned char centerValue = grayscaleImage(x, y);
			int censusCode = 0;
			for (int offsetY = -verticalRadius_; offsetY <= verticalRadius_; ++offsetY) {
				for (int offsetX = -horizontalRadius_; offsetX <= horizontalRadius_; ++offsetX) {
					if (y + offsetY >= 0 && y + offsetY < height
						&& x + offsetX >= 0 && x + offsetX < width
						&& grayscaleImage(x + offsetX, y + offsetY) >= centerValue) censusCode += 1;
					censusCode = censusCode << 1;
				}
			}
			censusImage(x, y) = censusCode;
		}
	}
}
