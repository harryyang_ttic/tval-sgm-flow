// Build
1) Type 'cmake .'
2) Type 'make'


// SGM-Stereo
Usage: ./bin/sgmstereo imageL.png imageR.png

Inputs:
   imageL.png: left image
   imageR.png: right image

Output:
   imageL_left_disparity.png: left disparity image (KITTI format: 16bit grayscale multiplied by 256)
   imageR_right_disparity.png: right disparity image (KITTI format: 16bit grayscale multiplied by 256)

// StereoSLIC
Usage: ./bin/stereoslic imageL.png imageR.png dispL.png dispR.png

Inputs:
   imageL.png: left image
   imageR.png: right image
   dispL.png: left disparity image
   dispR.png: right disparity image

Output:
   imageL_slic.png: segmentation image (16bit grayscale)
   imageL_slic_disparity.png: disparity image
   imageL_slic_boundary.png: boundary image


// Fundamental matrix estimation
Usage: ./bin/siftfund image0.png image1.png

Inputs:
   image0.png: first image
   image1.png: second image

Output:
   image0_fund.dat: fundamental matrix data file

// SGM-Flow
Usage: ./bin/sgmflow -i image0.png image1.png calib.txt fund.dat

Inputs:
   image0.png: first image
   image1.png: second image
   calib.txt: calibration file
   fund.dat: fundamental matrix data file

Output:
   image0_flow.dat: flow image
Inputs:
   image0.png: first image
   image1.png: second image

Output:
   image0_flow.png: flow image
   image0_first_vz.png: first VZ-ratio image
   image0_second_vz.png: second VZ-ratio image

// MotionSLIC
Usage: ./bin/motionslic image0.png image1.png vz0.png vz1.png calib.txt fund.dat

Inputs:
   image0.png: first image
   image1.png: second image
   vz0.png: first VZ-ratio image
   vz1.png: second VZ-ratio image
   calib.txt: calibration file
   fund.dat: fundamental matrix data file

Output:
   image0_slic.png: segmentation image
   image0_vz.png: VZ-ratio image
   image0_flow.png: flow image
   image0_boundary.png: boundary image


// Alpha estimation
Usage: ./bin/estalpha disp.png flow.png calib.txt fund.dat out_mot.dat

Inputs:
   disp.png: dispairty image (Output of SGM-Stereo)
   flow.png: flow image (Output of SGM-Flow)
   calib.txt: calibration file
   fund.dat: fundamental matrix data file

Output:
   out_mot.dat: motion data file

// SGM-StereoFlow
Usage: ./bin/sgmstereoflow imageL0.png imageR0.png imageL1.png disp.png flow.png mot.dat

Inputs:
   imageL0.png: left image at time t
   imageR0.png: right image at time t
   imageL1.png: left image at time t+1
   disp.png: disparity image (Output of SGM-Stereo)
   flow.png: flow image (Output of SGM-Flow)
   mot.dat: motion data file

Output:
   imageL0_left_disparity.png: disparity image
   imageL0_flow.png: flow image

// Smooth fit
Usage: ./bin/sgmstereoflow imageL0.png disp.png mot.dat

Inputs:
   imageL0.png: left image at time t
   disp.png: disparity image (Output of SGM-StereoFlow)
   mot.dat: motion data file

Output:
   imageL0_seg.png: segmentation image
   imageL0_seg_disparity.png: disparity image
   imageL0_seg_flow.png: flow image
   imageL0_seg_boundary.png: boundary image
   imageL0_seg_label.png: boundary label image
   imageL0_seg_label.txt: boundary label data file
 